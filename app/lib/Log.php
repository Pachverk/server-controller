<?php


namespace Pachverk;


class Log
{
    static private $errors  = [];
    static private $success = [];
    static private $log     = [];

    static function setError($log)
    {
        self::$errors[] = $log;
    }

    static function printAll()
    {
        if (!empty(self::$log)) {
            echo 'Лог:'.PHP_EOL;
            foreach (self::$log as $log) {
                if (is_string($log)) echo $log.PHP_EOL;
                else print_r($log);
            }
        }

        if (!empty(self::$success)) {
            echo 'Успешно:'.PHP_EOL;
            foreach (self::$success as $success)
                echo $success.PHP_EOL;
        }

        if (!empty(self::$errors)) {
            echo 'Ошибки:'.PHP_EOL;
            foreach (self::$errors as $error)
                echo $error.PHP_EOL;
        }
    }

    static function setSuccess($log)
    {
        self::$success[] = $log;
    }

    static function setLog($log)
    {
        self::$log[] = $log;
    }

    static function getErrors()
    {
        return self::$errors;
    }

    static function getLog()
    {
        return self::$log;
    }

    static function getSuccess()
    {
        return self::$success;
    }
}