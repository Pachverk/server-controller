<?php
/**
 * Неконструктивную критику игнорирую,
 * принимаю более логические альтернативы
 */

namespace Pachverk;


class Backup
{
    private $tarName;
    private $arFilesDirs;
    static $backupDir = '/tmp/backups';

    static function init()
    {
        return new self;
    }

    public function create()
    {
        $arh = implode(' ', $this->arFilesDirs);
        $res = shell_exec("tar -cvf {$this->tarName} {$arh}");
        return $res;
    }

    /**
     * Создание бэкапа баз данных и заброс на фтпшник
     * @return bool
     */
    static function backupMysql() {
        // Переменная для ручного теста
        $step = [
            'createDirForDumps'   => true,
            'createDump'          => true,
            'createTar'           => true,
            'putTarToServer'      => true,
            'deletePutFile'       => true,
            'deleteOldFilesOnFtp' => true,
        ];
        $hostName   = exec("hostname");
        $date       = date('d.m.Y.');
        $tar        = self::$backupDir.'/'.$date.'mysqls.tar';
        $backupsDir = self::$backupDir.'/mysql';

        if (!Mysql::init()) return false;

        // Создание директории для дампов
        if ($step['createDirForDumps']) {
            mkdir($backupsDir, 0755, true);
        }

        // Создание дампов
        if ($step['createDump']) {
            foreach (Mysql::getDatabases() as $dbName) {
                $log = shell_exec("mysqldump -u root $dbName > $backupsDir/$dbName.sql;").PHP_EOL;

                if (!empty(trim($log))) {
                    Log::setError($dbName.': '.$log);
                } else {
                    Log::setSuccess($dbName.': Успешно создан думп файл');
                }
            }
        }

        // Создание архива
        if ($step['createTar']) {
            if (1) {
                $comand = "tar -cvf $tar $backupsDir";
                exec($comand);
            }
        }

        // Отправка в хранилище
        if ($step['putTarToServer'] && file_exists($tar)) {
            FTP::sftpPutFile($tar, '/'.$hostName.'/mysql/'.$date.'mysqls.tar', $step['deletePutFile']);
            if ($step['deleteOldFiles'] && $ftp = FTP::ini()) {
                $ftp->deleteOldFiles('/'.$hostName.'/mysql/');
                $ftp->closeConnect();
            }
        }
    }

    /**
     * Бекап делается без учета базы данных
     */
    static function createBackups() {
        /**
         * @var Site $site
         */
        $hostName = exec('hostname');
        $hostName = trim($hostName);
        $siteList = Httpd::getSiteList();
        foreach ($siteList as $site) {

            if (!$site->documentRoot) {
                Log::setError("$site->id: У сайта не существует директории с сайтом");
                continue;
            }

            if ($site->createBackup()) {
                FTP::sftpPutFile($site->backupPath,  '/'.$hostName.'/'.$site->id.'/'.basename($site->backupPath));

                if ($ftp = FTP::ini()) {
                    $ftp->deleteOldFiles('/'.$hostName.'/'.$site->id.'/', Settings::getOption('ftpCountBackupsSave'));
                }
            }
        }

        if ($ftp) $ftp->closeConnect();
    }

    public function setTarName($tarName)
    {
        $this->tarName = $tarName;
        return $this;
    }
}