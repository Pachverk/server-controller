<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 27.06.2019
 * Time: 14:11
 */

namespace Pachverk;


use Bitrix\Main\UI\Uploader\Log;

class UnitTest
{
    public $arStatus;
    static function run()
    {
        $ob = new self();
        // $ob->unixUsers();
        $ob->mysql();
        $ob->ftp();
        // $ob->webSpace(); // TODO Тест в сыром виде запускать нельзя
        $ob->printArStatus();
    }

    public function unixUsers() {
        $unixUserForTest = 'unittester';
        $this->setStatus(class_exists(UnitTest::class), 'Наличие нужного класса');
        $this->setStatus(Linux::createUser($unixUserForTest), 'Статус метода создания пользователя Linux::createUser');
        $this->setStatus(Linux::checkEmptyUser($unixUserForTest) === false, 'Проверка через инструмен на то удалось ли создать пользователя');
        clearstatcache();
        $this->setStatus(is_dir("/home/$unixUserForTest"), 'Проверка наличия директории у пользователя в системе');
        $this->setStatus(Linux::deleteUser($unixUserForTest), 'Статус метода по удаление пользователя');
        clearstatcache();
        $this->setStatus(!is_dir("/home/$unixUserForTest"), "Проверка не осталось ли директории от пользователя (/home/$unixUserForTest)");
        $this->setStatus(Linux::checkEmptyUser($unixUserForTest),'Проверка на то удалось ли удалить пользователя из системы через внутренний инструмент');
    }

    public function mysql() {
        $dataConnect = Mysql::init();
        $this->mysqlParams();
        $this->setStatus($dataConnect, 'Тест соединения с базой данных');
        if ($dataConnect) {
            $this->mysqlGetUsers();
            $this->mysqlGetDatabases();
            $this->mysqlGenerateDBName();
            $this->mysqlCreateDBAndUser();
            $this->mysqlDeleteDBAndUser();
        }

    }
    public function mysqlParams() {
        $login = Settings::getOption('mysqlLogin');
        $this->setStatus(!empty($login), "Параметр для соединения с базой данных ({$login})");
        $this->setStatus(!empty(Settings::getOption('mysqlPassWord')), 'Пароль для базы данных успешно обявлен');
    }
    public function mysqlGetUsers() {
        $arUsers = Mysql::getUsers();
        $status = (is_array($arUsers) && count($arUsers) > 1 && in_array('root', $arUsers));
        $this->setStatus($status, 'Тест получения списка пользователей');
    }
    public function mysqlGetDatabases() {
        $arDBs = Mysql::getDatabases();
        $status = (is_array($arDBs)
            && count($arDBs) > 2
            && in_array('information_schema', $arDBs)
            && in_array('mysql', $arDBs)
        );
        $this->setStatus($status, 'Получение списка баз данных');
    }
    public function mysqlGenerateDBName() {
        $siteID = 'some.test.ua';
        $name = Mysql::getDBNameBySiteID($siteID);
        $staus = (!empty($name));
        $this->setStatus($staus, "Генерация имени базы данных ({$name})");
    }
    public function mysqlCreateDBAndUser($user='gtest', $db='gtest', $password = 'testSuccess!') {
        $this->setStatus(Mysql::createDatabase($db, $user, $password), "Создание базы даныых login:$user password:$password db:$db");
        $this->setStatus((in_array($user, Mysql::getUsers())), "Проверка на то есть ли уже пользователь в базе ($user)");
        $this->setStatus((in_array($db, Mysql::getDatabases())), "Создалась ли база данных успешно ($db)");
    }
    public function mysqlDeleteDBAndUser($user='gtest', $db='gtest') {
        $this->setStatus(Mysql::deleteUser($user), "Статус удаления пользователя ($user)");
        $this->setStatus(Mysql::deleteDatabase($db), "Статус удаления базы данных ($db)");
    }

    public function webSpace() {
        $site = new Site();
        $site->id = 'unit.test.com';
        $site->domains = ['unit.test.com', 'www.unit.test.com'];
        $site->linuxUser = 'unittest';
        $site->dbUser = $site->dbName = "unittest";
        $site->dbPassword = 'unittest.password';
        $statusHttpd = $site->generateHttpdFile();
        $statusNginx = $site->generateNginxFile();

        $this->setStatus($statusHttpd, 'Сосотояние метода который должен создать конфиг файл Httpd');
        $this->setStatus(!empty($site->httpdConfigFile) && file_exists($site->httpdConfigFile), "Существует ли конфиг файл ({$site->httpdConfigFile})");
        $data = file_get_contents($site->httpdConfigFile);
        $this->setStatus(!empty($data), 'Содержимое файла: '.PHP_EOL.$data);
        unlink($site->httpdConfigFile);

        $this->setStatus($statusNginx, 'Сосотояние метода который должен создать конфиг файл Nginx');
        $this->setStatus(!empty($site->nginxConfigFile) && file_exists($site->nginxConfigFile), "Существует ли конфиг файл ({$site->nginxConfigFile})");
        $data = file_get_contents($site->nginxConfigFile);
        $this->setStatus(!empty($data), 'Содержимое файла: '.PHP_EOL.$data);
        unlink($site->nginxConfigFile);
    }

    /**
     * Собирает статусы обработок
     * @var bool $status
     * @var string $text
     */
    public function setStatus($status, $text) {
        $this->arStatus[] = [
            'status' => $status,
            'message' => $text
        ];
    }
    public function printArStatus() {
       foreach ($this->arStatus as $event) {
           //
           echo ($event['status']
               ? "\e[32mOK\e[0m - {$event['message']}"
               : "\e[31mFail\e[0m - {$event['message']}")
               . PHP_EOL;
       }
    }

    public function ftp() {
        $this->ftpParams();
        $this->ftpConnect();
    }
    public function ftpParams() {
        $serv = Settings::getOption('ftpServerName');
        $user = Settings::getOption('ftpLogin');
        $pass = Settings::getOption('ftpPassWord');

        $this->setStatus(($serv) ? true : false, "Сервер ftp ({$serv})");
        $this->setStatus(($user) ? true : false, "Пользователь для подключения ftp ({$user})");
        $this->setStatus(($pass) ? true : false, 'Пароль к ftp серверу');
    }
    public function ftpConnect() {
        $this->setStatus(FTP::testConnect(), 'Проверка установки соединения');
    }
}