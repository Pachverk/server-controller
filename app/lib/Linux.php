<?php


namespace Pachverk;


class Linux
{
    static function deleteUser($user, $flag='-r') {
        if (empty($user)) {
            Log::setError('Пользователь для создания в системы линукс не указан');
            return false;
        }

        exec("userdel $flag $user");
        return true;
    }

    static function createUser($user, $homeDir='') {
        if (empty($user)) {
            Log::setError('Пользователь для создания в системы линукс не указан');
            return false;
        }

        if (self::checkEmptyUser($user) === false) {
            Log::setError("Пользователь в системе уже зарезервирован ($user)");
            return false;
        }

        if (!empty($homeDir)) {
            exec("adduser $user --home $homeDir");
            chmod($homeDir, 0700);
        } else {
            exec("adduser $user");
        }

        if (self::checkEmptyUser($user) === false || !is_dir($homeDir)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Проверяет на уникальность имя пользователя в линукс системе
     * https://losst.ru/kak-posmotret-spisok-polzovatelej-v-ubuntu
     * @param $userName
     * @return bool
     */
    static function checkEmptyUser($userName) {
        $users = trim(shell_exec('sed \'s/:.*//\' /etc/passwd'));
        $users = explode("\n", $users);
        return (in_array($userName, $users)) ? false : true;
    }
}