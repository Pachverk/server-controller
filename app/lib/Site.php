<?php


namespace Pachverk;


use Bitrix\Main\UI\Uploader\Log;

class Site
{
    public $id;
    public $ip;
    public $documentRoot;
    public $domains;
    /** @var Server */
    public $server;
    /** @var bool Состояние сайта (Включен или выключен) */
    public $status = true;

    public $encoding = 'utf8';
    public $dirSessions;
    public $dirUploads;
    public $httpdConfigFile;
    /** @var string Эмейл от имени кого будут делаться отправка писем */
    public $mailSender;

    public $nginxConfigFile;
    /** @var  */
    public $sslFile;
    // public $nginxSSLConfigFile; // Todo Нужно избавиться от этого формата

    public $errors = [];
    public $warnings = [];

    public $dbName;
    public $dbUser;
    public $dbPassword;

    /**
     * Директория в которой буду сложены бекапы по ID сайта
     * @var string
     */
    public $backupDir = '/tmp/backups';

    /**
     * @todo Нежно расписать иначе может запутать
     * @var string
     */
    public $backupPath;

    /** @var string $linuxUser Пользователь в системе линукс */
    public $linuxUser;

    /**
     * @todo Нужно избавиться от этого места (Другим сайтам не на Bitrix оно не нужно)
     * @var array Директории которые нужно прочистить перед формированием бекапа
     */
    public $clerDirsInSite = [
        '/bitrix/backup',
        '/bitrix/cache',
        '/bitrix/managed_cache',
        '/bitrix/stack_cache',
    ];

    /**
     * Вернет перечень сайтов с окружения которорое видит httpd
     * @return array
     */
    static function getSites() {
        /** @var Site $site */
        $sites = Httpd::getSiteList();
        foreach ($sites as $site) {
            $sslFile = Nginx::getPathSslFile($site->id);
            if (file_exists($sslFile)) {
                $site->sslFile = $sslFile;
            }
        }
        return $sites;
    }

    public function getSize() {
        return Tools::getDirSize($this->documentRoot);
    }

    public function createBackup()
    {
        if ($this->validate()) {
            $base = "{$this->backupDir}/{$this->id}";
            $backupDir = $this->documentRoot.'/bitrix/backup';

            // Создаие места для хранения бекапа
            if (1 && !is_dir($base)) {
                if (!mkdir($base, 755, true)) {
                    if (class_exists(Log::class))
                        Log::setError('Не удалось создать директорию для хранения бекапов');
                    return false;
                }
            }

            // Очистка от мусора перед бэкапированием
            if (1) {
                foreach ($this->clerDirsInSite as $dir) {
                    exec("rm -rf {$this->documentRoot}$dir");
                }
                Tools::mkdir($backupDir, 0755, true, $this->linuxUser);
            }

            // Создать бэкап базы данных
            if (1 && $this->findDB()) {
                $dumpFile = $backupDir.'/'.date('d.m.Y.').$this->dbName.'.sql';
                Mysql::dump($this->dbName, $dumpFile);
                if (file_exists($dumpFile)) {
                    Log::setSuccess("[{$this->id}] Успешно создан $dumpFile");
                    chown($dumpFile, $this->linuxUser);
                    chgrp($dumpFile, $this->linuxUser);
                } else {
                    Log::setError("[{$this->id}] Дамп не создан $dumpFile");
                }
            }

            // Сохранения текущего состояния файла
            if (1) {
                $data = Core::compactData($this);
                file_put_contents($backupDir.'/settings.yam', $data);
                if (file_exists($backupDir.'/settings.yam')) {
                    chmod($backupDir.'/settings.yam', 0600);
                    Log::setSuccess("[{$this->id}] Успешно сохранены настройки сайта ($backupDir/settings.yam)");
                } else {
                    Log::setSuccess("[{$this->id}] Не удалось сохранить состояние настроек ($backupDir/settings.yam)");
                }
            }

            // Создание архива
            if (1) {
                $date       = date('d.m.Y.');
                $backupPath = "{$base}/{$date}{$this->id}.tar";
                exec("tar -cvf $backupPath {$this->documentRoot}");
                if (file_exists($backupPath)) {
                    $this->backupPath = $backupPath;
                    Log::setSuccess("[{$this->id}] Бэкапирование сайта прошло успешно, архив ($this->backupPath)");
                    // unlink($backupDir.'/settings.yam');
                    return true;
                }
            }
        }

        Log::setError("[{$this->id}] Бэкапирование сайта провалено");
        return false;
    }

    public function validate()
    {
        if (empty($this->documentRoot) || !is_dir($this->documentRoot)) {
            Log::setError("[{$this->id}] Не нашлось корневой директории сайта ({$this->documentRoot})");
            return false;
        }

        if (in_array(realpath($this->documentRoot), ['.', '..', '/', '/home'])) {
            Log::setError("[{$this->id}] У сайта не правильно указана директория его хранения ({$this->documentRoot})");
            return false;
        }

        if (empty($this->domains)) {
            Log::setError("[{$this->id}] У сайта не указано не одного домена");
            return false;
        }

        if (empty($this->linuxUser) || $this->linuxUser === 'root') {
            Log::setError("[{$this->id}] У сайта не корретно указан пользователь в линукс системе ({$this->linuxUser})");
            return false;
        }

        return true;
    }

    /**
     * Проверяет наличие конфиг файла у сайта
     * Если файл найден сохраняет его путь в объект
     */
    public function checkNginxFiles()
    {
        $nginx = Nginx::getPathConfigFile($this->id);
        if (file_exists($nginx)) {
            $this->nginxConfigFile = $nginx;
            if(!empty($this->errors['nginxFileExist'])) unset($this->errors['nginxFileExist']);
            return true;
        } else {
            $this->errors['nginxFileExist'] = "Файл с настройками для nginx отсутсвует ($nginx)";
        }
        return false;
    }

    /**
     * Сгенерирует nginx файл с конфигурациями
     */
    public function generateHttpdFile() {
        return Httpd::generateConfFile($this, true);
    }

    /**
     * Сгенерирует nginx файл с конфигурациями
     */
    public function generateNginxFile() {
        return Nginx::createNginxConfig($this, true);
    }

    /**
     * Генерирует нормальный путь к корню сайта там где он реально должен быть
     * Важно что бы обязательно был указан пользователь линукс в переменной
     * @return string $this->getDefaultSitesPath().'/'.$this->id
     */
    public function getDefaultDocumentRoot() {
        return $this->getDefaultSitesPath().'/'.$this->id;
    }

    /**
     * Путь где будет лежать папка с сайтом
     * @return string $this->getSpacePath()."/sites"
     */
    public function getDefaultSitesPath() {
        return $this->getSpacePath()."/sites";
    }

    /**
     * Вернет место где должны лежать все что относится к конкретному пользователю
     * @return string "/home/{$this->linuxUser}"
     */
    public function getSpacePath() {
        return "/home/{$this->linuxUser}";
    }

    static function getSiteByID($siteID)
    {
        /** @var Site $site */
        $list = self::getSites();
        foreach ($list as $site) {
            if ($site->id === $siteID) return $site;
        }
        return;
    }

    static function getSiteByDomain($domain) {
        /** @var Site $site */
        $sites = Httpd::getSiteList();
        foreach ($sites as $site) {
            if ($site->id == $domain
                || in_array($domain, $site->domains)
                || in_array('www.'.$domain, $site->domains)
            ) {
                return $site;
            }
        }
    }

    static function getSitesWithProblem() {

        /** @var Site $site */
        $sites = Httpd::getSiteList();
        $withProblems = [];
        foreach ($sites as $site) {
            $bug = false;
            $dir = [
                'modules' => $site->documentRoot.'/bitrix/modules',
                'bitrix' => $site->documentRoot.'/bitrix',
                'components' => $site->documentRoot."/bitrix/components/yamasters",
            ];

            if (is_link($dir['bitrix']) && !file_exists(readlink($dir['bitrix']))) {
                $site->errors[] = 'Основное ядро сайта в виде линка не подключено';
            }

            if (!$site->validate()) {
                $site->errors[] = 'Проблема с внутренней валидацией validate()';
                $bug = true;
            }

            if (is_link($dir['components']) && !file_exists(readlink($dir['components']))) {
                $site->errors[] = 'У сайта отсутствует его оригинальные компоненты /bitrix/components/yamasters';
                $bug = true;
            }

            if (strpos($site->documentRoot, 'bitrix/ext_www') !== false) {
                $site->errors[] = 'Сайт находится в потенциально поасной директории';
                $bug = true;
            }

            if ($site->documentRoot !== $site->getDefaultDocumentRoot()) {
                $site->errors['structureFail'] = 'Сайт не соотвествует стандарту для сервера нужно подправить '.$site->documentRoot;
                $bug = true;
            }

            if (!$site->findDB()) {
                $site->errors['dbEmptyDBName'] = 'У сайта сложности с определение родной базы данных';
                $bug = true;
            }

            if (!$site->checkNginxFiles()) {
                $site->errors['nginxStructure'] = 'Файл с настройками nginx не нашелся';
                $bug = true;
            }

            /*$file = $site->documentRoot.'/bitrix/php_interface/after_connect.php';
            if (file_exists($file)) {
                $data = file_get_contents($file);
                if (strpos($data, 'cp1251') !== false && in_array($site->encoding , ['utf8', 'utf-8'])) {
                    $site->errors['needChangeEncode'] = 'Сайту нажно сменить кодировку для 1251'.PHP_EOL.$data;
                    $bug = true;
                }
            }*/

            if ($bug) {
                $withProblems[] = $site;
                continue;
            }
        }
        return $withProblems;
    }

    /**
     * Путь к сессиям
     * @return string $this->getSpacePath().'/sessions'
     */
    public function getSessionsDir() {
        return $this->getSpacePath().'/sessions';
    }

    public function getUploadsDir() {
        return $this->getSpacePath().'/uploads';
    }

    public function modifierDomains() {
        $needChange = false;
        foreach ($this->domains as $domain) {
            if (strpos($domain, 'www.') === false && !in_array('www.'.$domain, $this->domains)) {
                $this->domains[] = 'www.'.$domain;
                $needChange = true;
            }

            if (strpos($domain, 'www.') !== false) {
                $newDomain = str_replace('www.', '', $domain);
                if (!in_array($newDomain, $this->domains)) {
                    $this->domains[] = $newDomain;
                    $needChange = true;
                }
            }
        }
        $this->domains = array_unique($this->domains);
        return $needChange;
    }

    public function createSpace() {
        if (empty($this->id)) {
            Log::setError("Для создания окружения сайта необходимо указать id сайта");
            return false;
        }

        // if (!self::checkEmptySiteID($this->id)) {
        //     Loger::setError("Сайт с таким id($this->id) уже существует");
        //     return false;
        // }

        if (empty($this->documentRoot)) {
            Log::setError("Не указана директория к сайту ({$this->id})");
            return false;
        }

        // if (empty($this->httpdConfigFile) || !file_exists($this->httpdConfigFile)) {
        //     Loger::setError("Для корретной работы и создания окружения необходимо создать файл конфигурационный файл {$this->httpdConfigFile}");
        //     return false;
        // }

        if (empty($this->linuxUser)) {
            Log::setError("Для создания окружения сайта нужно указать пользователя linux");
            return false;
        }

        if (1) {
            Linux::createUser($this->linuxUser);
            mkdir($this->documentRoot, 0755, true);
            $this->createSessionsDir();
            $this->createUploadsDir();
            exec("chown {$this->linuxUser}:{$this->linuxUser} ".$this->getSpacePath());
            exec("chown -R {$this->linuxUser}:{$this->linuxUser} ".$this->getDefaultDocumentRoot());
        }

        $this->generateHttpdFile();
        $this->generateNginxFile();
        return true;
    }

    public function createSessionsDir()
    {
        if (!is_dir($this->getSpacePath())) {
            Log::setError('для создания директории с сессиями нет окружением сайта');
            return false;
        }

        $this->dirSessions = $this->getSessionsDir();

        mkdir($this->dirSessions, 0755);
        if ($this->linuxUser) {
            exec("chown {$this->linuxUser}:{$this->linuxUser} {$this->dirSessions}");
        }

        return true;
    }

    public function createUploadsDir()
    {
        if (!is_dir($this->getSpacePath())) {
            Log::setError('для создания директории с загрузками нет окружением сайта');
            return false;
        }

        $this->dirUploads = $this->getUploadsDir();

        mkdir($this->dirUploads, 0755);
        if ($this->linuxUser) {
            exec("chown {$this->linuxUser}:{$this->linuxUser} {$this->dirUploads}");
        }

        return true;
    }

    /**
     * Удаляет сожержимое сайта (имеет проверки что бы не снести лишнего)
     * @return bool
     */
    public function deleteDocumentRoot() {
        if (!empty($this->documentRoot)
        && !empty($this->linuxUser)
        && strpos("/home/{$this->linuxUser}/sites/", $this->documentRoot) !== false
        && is_dir($this->documentRoot)
        ) {
            exec("rm -rf {$this->documentRoot}");
            return true;
        }
        return false;
    }

    public function deleteTrashDomains() {
        $this->domains = array_unique($this->domains);
        foreach ($this->domains as $key => $domain) {
            if (strpos($domain, 'yamasters.com') !== false) {
                $sub = explode('.', $domain);
                if (count($sub) > 3) {
                    unset($this->domains[$key]);
                }
            }
        }
    }

    static function getDefaultUsetNameByID($siteID) {
        if (empty($siteID)) return;
        $siteID = str_replace([
            '.net', '.ua', '.com', '.kiev', '.ru', '.org', '.pl', '.yamasters',
           '.', '-',
        ], '', $siteID);

        $siteID = preg_replace('/[^a-z]/', '', $siteID);
        return $siteID;
    }

    /**
     * Проверка на уникальность идентификатора сайта
     * @param $siteID
     * @return bool
     */
    static function checkEmptySiteID($siteID) {
        /**
         * @var Site $site
         */
        $sites = Httpd::getSiteList();
        foreach ($sites as $site) {
            if ($site->id == $siteID) {
                return false;
            }
        }
        return true;
    }

    static function checkEmptyDomains($arDomains) {
        /**
         * @var Site $site
         */
        $sites = Httpd::getSiteList();
        foreach ($sites as $site) {
            foreach ($arDomains as $domain) {
                if (in_array($domain, $site->domains)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param array $arDomains Список доменов которые нужно проверить
     * @return array Перечень доменов которые уже зарезервированые
     */
    static function getDomainsAlreadyExists($arDomains) {
        $arExists = [];
        /**
         * @var Site $site
         */
        $sites = Httpd::getSiteList();
        foreach ($sites as $site) {
            foreach ($arDomains as $domain) {
                if (in_array($domain, $site->domains) || $site->id == $domain) {
                    $arExists[] = $domain;
                }
            }
        }
        return $arExists;
    }

    public function disableSite() {
        if ($this->status === true) {
            Log::setLog($this);
            Httpd::disableSite($this);
            Nginx::deleteAllConfingsBySite($this);
        }
    }

    public function activeteSite() {
        if ($this->status === false) {
            Log::setLog($this);
            Httpd::activeteSite($this);
            $this->generateNginxFile();
        }
    }

    /**
     * Правки будут происходить только на уровне окружения
     * Будут изменены конфиги на основе данных которые видны в текучей точке
     * @return bool
     */
    public function update() {
        $real = self::getSiteByID($this->id);
        if ($real->id != $this->id) {
            Log::setError('Не удалось обновить сайт на єтапе сравнения информации');
            return false;
        }

        $real->domains = $this->domains;
        $real->mailSender = $this->mailSender;
        $real->documentRoot = $this->documentRoot;
        $real->linuxUser = $this->linuxUser;
        $real->encoding = $this->encoding;

        if ($real->validate()) {

            if (1) {
                $httpdStatus = Httpd::generateConfFile($real, true);
                if (!$httpdStatus) {
                    return false;
                }
            }

            if ($this->status === $real->status) {
                $real->generateNginxFile();
            }

            if ($this->status === true) {
                $real->activeteSite();
            }

            if ($this->status === false) {
                $real->disableSite();
            }

        }

        return true;
    }

    /**
     * Удаление сайта в ходе будет:
     * - Сохранен полный бэкап
     * - Заброшен на фтп сервер
     * - Удалены конфиг файлы и очищена директория сайта
     * - Удалена база данных
     * @return bool
     */
    public function delete() {
        if (!$this->validate()) return false;
        $debug = false;
        $imitateCreatedBackup = false;
        $delete = true;
        $deleteDB = true;

        if ($imitateCreatedBackup || $this->createBackup()) {
            if ($imitateCreatedBackup || FTP::sftpPutFile($this->backupPath,  '/site-is-deleted/'.basename($this->backupPath))){
                if ($delete) {
                    $comand = "rm -rf {$this->documentRoot}";
                    Log::setLog($comand);
                    if (!$debug) exec($comand);

                    if (!empty($this->httpdConfigFile) && file_exists($this->httpdConfigFile)) {
                        if (!$debug) unlink($this->httpdConfigFile);
                        Log::setLog("unlink {$this->httpdConfigFile}");
                    }

                    if ($this->checkNginxFiles()) {
                        if (!$debug) unlink($this->nginxConfigFile);
                        Log::setLog("unlink {$this->nginxConfigFile}");
                    }

                    if ($deleteDB && Mysql::init()) {
                        Mysql::deleteDatabase($this->dbName);
                    }

                    if (!$debug) {
                        Log::setLog('Restart server');
                        Httpd::restart();
                        Nginx::restart();
                    }

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Попытка определить у конкретного сайта его базу данных
     * Совпадение будет хранится в $this->dbName
     * @return bool
     */
    public function findDB() {
        $databasesFind = [];
        $dbconn = $this->documentRoot.'/bitrix/php_interface/dbconn.php';
        $databases = (Mysql::init()) ? Mysql::getDatabases() : null;

        if (empty($databases)) {
            Log::getErrors("Возникла проблема получения списка баз данных");
            return false;
        }

        if (!file_exists($dbconn)) {
            Log::setError("[{$this->id}] У сайта не нашлось файла с подключением к базе данных ($dbconn)");
            return false;
        }


        $data = file_get_contents($dbconn);
        foreach ($databases as $database) {
            if (in_array($database, ['mysql', 'information_schema'])) continue;
            $re = "/['\"]{$database}[\"']/m";
            if ($res = preg_match_all($re, $data)) {
                $databasesFind[] = $database;
            }
            // if (strpos($data, $database) !== false) {
            //     $databasesFind[] = $database;
            // }
        }


        if (count($databasesFind) == 1) {
            // TODO Сохранение названия базы данных
        } elseif(count($databasesFind) > 1) {
            Log::setError("[{$this->id}] У сайта нашлось более одной базы которая может совпадть (".implode(', ', $databasesFind)."). Проверьте корректность файла {$dbconn}");
            return false;
        } else {
            Log::setError("[{$this->id}] У сайта проблемы, не нашлось нужного совпадения базы данных");
            return false;
        }

        $this->dbName = current($databasesFind);
        return true;
    }
}