<?php


namespace Pachverk;


class Httpd
{
    private static $pathSites       = '/etc/httpd/bx/conf';
    private static $pathSaveConfigs = '/etc/httpd/old.conf';

    /**
     * Получает путь к конфиг файлу сайта по умолчанию
     * @param string $siteID
     * @param bool $status
     * @return string
     */
    static function getConfigPathDefault($siteID, $status=true) {
        if (empty($siteID)) {
            Log::setError('Не указан ID сайта');
            return;
        }

        if ($status) {
            $file = "/etc/httpd/bx/conf/bx_ext_{$siteID}.conf";
        } else {
            $file = "/etc/httpd/bx/conf/bx_ext_{$siteID}.disabled";
        }

        return $file;
    }

    /**
     * @param Site $site
     *                  Используются такие параметры как (id, domains, linuxUser, documentRoot, mailSender, )
     * @param bool $update
     * @return bool
     */
    static function generateConfFile(Site $site, $update=false) {
        if ($update !== true && file_exists($site->httpdConfigFile)) {
            Log::setError('Файл с конфигами httpd уже существует');
            return false;
        }

        if (empty($site->httpdConfigFile)) {
            $site->httpdConfigFile = self::getConfigPathDefault($site->id);
        }

        if (empty($site->domains)) {
            Log::setError('Для создания конфиг файла httpd нужно указать домены');
            return false;
        }

        if (empty($site->linuxUser)) {
            Log::setError('Для создание конфигов httpd сайта нужно указать пользователя в линукс системе');
            return false;
        }

        $domains = implode(' ', $site->domains);
        $site->documentRoot = "/home/{$site->linuxUser}/sites/{$site->id}";
        $site->dirSessions = "/home/{$site->linuxUser}/sessions";
        $site->dirUploads = "/home/{$site->linuxUser}/uploads";

        $template = '';
        $template .= "<VirtualHost 127.0.0.1:8887>\n";
        $template .= "\tServerName  {$site->id}\n";
        $template .= "\tServerAlias {$domains}\n";
        $template .= "\tServerAdmin webmaster@localhost\n";
        $template .= "\tDocumentRoot {$site->documentRoot}\n";
        $template .= "\n";
        $template .= "\tErrorLog logs/{$site->id}_error_log\n";
        $template .= "\tLogLevel warn\n";
        $template .= "\tCustomLog logs/{$site->id}_access_log combined\n";
        $template .= "\n";
        $template .= "\t<IfModule mod_rewrite.c>\n";
        $template .= "\t	#Nginx should have \"proxy_set_header HTTPS YES;\" in location\n";
        $template .= "\t	RewriteEngine On\n";
        $template .= "\t	RewriteCond %{HTTP:HTTPS} =YES\n";
        $template .= "\t	RewriteRule .* - [E=HTTPS:on,L]\n";
        $template .= "\t</IfModule>\n";
        $template .= "\n";
        if (in_array($site->encoding, ['windows-1251'])) {
            $template .= "\tphp_admin_value mbstring.func_overload 0\n";
        } else {
            $template .= "\tphp_admin_value mbstring.func_overload 2\n";
        }
        $template .= "\t# php_admin_value mbstring.internal_encoding utf-8\n";
        $template .= "\n";
        $template .= "\t<IfModule mpm_itk_module>\n";
        $template .= "\t	AssignUserID {$site->linuxUser} {$site->linuxUser}\n";
        $template .= "\t</IfModule>\n";
        $template .= "\n";
        $template .= "\t<Directory />\n";
        $template .= "\t	Options FollowSymLinks\n";
        $template .= "\t	AllowOverride None\n";
        $template .= "\t</Directory>\n";
        $template .= "\n";
        $template .= "\t<DirectoryMatch .*\.svn/.*>\n";
        $template .= "\t	 Deny From All\n";
        $template .= "\t</DirectoryMatch>\n";
        $template .= "\n";
        $template .= "\t<DirectoryMatch .*\.git/.*>\n";
        $template .= "\t	 Deny From All\n";
        $template .= "\t</DirectoryMatch>\n";
        $template .= "\n";
        $template .= "\t<DirectoryMatch .*\.hg/.*>\n";
        $template .= "\t	 Deny From All\n";
        $template .= "\t</DirectoryMatch>\n";
        $template .= "\n";
        $template .= "\t<Directory {$site->documentRoot}>\n";
        $template .= "\t	Options Indexes FollowSymLinks MultiViews\n";
        $template .= "\t	AllowOverride All\n";
        $template .= "\t	DirectoryIndex index.php index.html index.htm\n";
        $template .= "\t	Order allow,deny\n";
        $template .= "\t	allow from all\n";
        $template .= "\t	php_admin_value session.save_path {$site->dirSessions}\n";
        $template .= "\t	php_admin_value upload_tmp_dir    {$site->dirUploads}\n";
        if ($site->mailSender) {
            $template .= "\t	php_admin_value sendmail_path \"/usr/sbin/sendmail.postfix -i -t -f {$site->mailSender}\"\n";;
        } else {
            $template .= "\t	php_admin_value sendmail_path \"/usr/sbin/sendmail.postfix -i -t -f info@{$site->id}\"\n";;
        }
        $template .= "\t</Directory>\n";
        $template .= "\n";
        $template .= "\t<Directory {$site->documentRoot}/bitrix/cache>\n";
        $template .= "\t	AllowOverride none\n";
        $template .= "\t	Order allow,deny\n";
        $template .= "\t	Deny from all\n";
        $template .= "\t</Directory>\n";
        $template .= "\n";
        $template .= "\t<Directory {$site->documentRoot}/bitrix/managed_cache>\n";
        $template .= "\t	AllowOverride none\n";
        $template .= "\t	Order allow,deny\n";
        $template .= "\t	Deny from all\n";
        $template .= "\t</Directory>\n";
        $template .= "\n";
        $template .= "\t<Directory {$site->documentRoot}/bitrix/local_cache>\n";
        $template .= "\t	AllowOverride none\n";
        $template .= "\t	Order allow,deny\n";
        $template .= "\t	Deny from all\n";
        $template .= "\t</Directory>\n";
        $template .= "\n";
        $template .= "\t<Directory {$site->documentRoot}/bitrix/stack_cache>\n";
        $template .= "\t	AllowOverride none\n";
        $template .= "\t	Order allow,deny\n";
        $template .= "\t	Deny from all\n";
        $template .= "\t</Directory>\n";
        $template .= "\n";
        $template .= "\t<Directory {$site->documentRoot}/upload>\n";
        $template .= "\t	AllowOverride none\n";
        $template .= "\t	AddType text/plain php,php3,php4,php5,php6,phtml,pl,asp,aspx,cgi,dll,exe,ico,shtm,shtml,fcg,fcgi,fpl,asmx,pht\n";
        $template .= "\t	php_value engine off\n";
        $template .= "\t</Directory>\n";
        $template .= "\n";
        $template .= "\t<Directory {$site->documentRoot}/upload/support/not_image>\n";
        $template .= "\t	AllowOverride none\n";
        $template .= "\t	Order allow,deny\n";
        $template .= "\t	Deny from all\n";
        $template .= "\t</Directory>\n";
        $template .= "\n";
        $template .= "\t<Directory {$site->documentRoot}/bitrix/images>\n";
        $template .= "\t	AllowOverride none\n";
        $template .= "\t	AddType text/plain php,php3,php4,php5,php6,phtml,pl,asp,aspx,cgi,dll,exe,ico,shtm,shtml,fcg,fcgi,fpl,asmx,pht\n";
        $template .= "\t	php_value engine off\n";
        $template .= "\t</Directory>\n";
        $template .= "\n";
        $template .= "\t<Directory {$site->documentRoot}/bitrix/tmp>\n";
        $template .= "\t	AllowOverride none\n";
        $template .= "\t	AddType text/plain php,php3,php4,php5,php6,phtml,pl,asp,aspx,cgi,dll,exe,ico,shtm,shtml,fcg,fcgi,fpl,asmx,pht\n";
        $template .= "\t	php_value engine off\n";
        $template .= "\t</Directory>\n";
        $template .= "\n";
        $template .= "</VirtualHost>";;

        if (!file_put_contents($site->httpdConfigFile, $template)) {
            Log::setError("Не удалось созранить конфифги для сайта {$site->id}");
            return false;
        }
        return true;
    }

    static function saveConfig($file)
    {
        $fileBack = self::$pathSaveConfigs . '/' . date('Y.m.d.') . basename($file);
        if (!file_exists($fileBack) && !copy($file, $fileBack)) {
            if (class_exists(Log::class)) Log::setError('Не удалось сохранить резервную копию настройки httpd ' . $file);
            return false;
        }
        return true;
    }

    /**
     * @todo Будет удалено аналогом
     * @param $file
     * @param $oldRootDir
     * @param $newRootDir
     * @return bool
     */
    static function changeRootDir($file, $oldRootDir, $newRootDir)
    {
        if (!file_exists($file) || !is_dir($oldRootDir) || !self::saveConfig($file)) {
            return false;
        }


        $fileBack = self::$pathSaveConfigs . '/' . date('Y.m.d.') . basename($file);
        if (!file_exists($fileBack) && !copy($file, $fileBack)) {
            if (class_exists(Log::class)) Log::setError('Не удалось сохранить резервную копию настройки httpd ' . $file);
            return false;
        }

        $fileData = file_get_contents($file);
        $fileData = str_replace($oldRootDir, $newRootDir, $fileData);
        if (!file_put_contents($file, $fileData)) {
            if (class_exists(Log::class)) Log::setError('Не удалось сохранить настройки httpd ' . $file);
            return false;
        }

        return true;
    }

    static function setDocumentRoot(Site $site, $documentRoot) {
        if (!self::saveConfig($site->httpdConfigFile)) {
            //TODO:: Дописать информацию логера
            return false;
        }

        $re = '/(DocumentRoot ).*/';
        $data = file_get_contents($site->httpdConfigFile);

        preg_match_all($re, $data, $matches, PREG_SET_ORDER, 0);
        $docRoot = current(current($matches));
        $docRoot = str_replace('DocumentRoot ', '', $docRoot);
        $docRoot = trim($docRoot);
        $data = str_replace($docRoot, $documentRoot, $data);

        if (!file_put_contents($site->httpdConfigFile, $data)) {
            if (class_exists(Log::class)) Log::setError('Не удалось сохранить конфиги с изменением сесси сайта ' . $site->id);
            return false;
        }

        return true;
    }

    static function setITKConf(Site $site, $linuxUserName)
    {
        if (!file_exists($site->httpdConfigFile) || !self::saveConfig($site->httpdConfigFile)) {
            return false;
        }

        $data = file_get_contents($site->httpdConfigFile);

        // ADD settings for httpd rout user
        if (strpos($data, "<IfModule mpm_itk_module>") === false) {
            $rout = "\n\t<IfModule mpm_itk_module>\n";
            $rout .= "\t\tAssignUserID {$linuxUserName} {$linuxUserName}\n";
            $rout .= "\t</IfModule>\n";
            $data = str_replace("</VirtualHost>", "$rout</VirtualHost>", $data);
        }

        file_put_contents($site->httpdConfigFile, $data);

        return true;
    }

    static function getSiteList()
    {
        $arSites = [];
        $files   = scandir(self::$pathSites);
        foreach ($files as $fileName) {
            if (in_array($fileName, ['.', '..']) || strpos($fileName, '.bak')) continue;
            if (strpos($fileName, 'bx_ext_') !== false) {
                $site                  = new Site;
                $site->id              = str_replace(['bx_ext_', '.conf', '.disabled'], '', $fileName);
                $site->httpdConfigFile = self::$pathSites . '/' . $fileName;

                $fileInfo = pathinfo($site->httpdConfigFile);
                if ($fileInfo['extension'] === 'disabled') {
                    $site->status = false;
                }

                $confFile = file_get_contents($site->httpdConfigFile);

                $functionOverload = Tools::getStringByWord($confFile, 'mbstring.func_overload');
                if ($functionOverload === '0') {
                    $site->encoding = 'windows-1251';
                }

                // $obSite->documentRoot
                if (1) {
                    $docRoot = self::getDocumentRoot($site);
                    if (is_dir($docRoot)) {
                        $site->documentRoot = $docRoot;
                    }
                }

                $site->dirSessions = self::getSessionsDir($site);
                $site->dirUploads = self::getUploadsDir($site);
                $site->mailSender = self::getMailSender($site);

                // Домены сайта
                if (1) {
                    $re = '/(ServerAlias ).*/';
                    preg_match_all($re, $confFile, $matches, PREG_SET_ORDER, 0);
                    $domains = current(current($matches));
                    $domains = str_replace('ServerAlias ', '', $domains);
                    $domains = trim($domains);
                    $site->domains = $domains;
                    $site->domains = explode(' ', trim($site->domains));
                }

                if (is_dir($site->documentRoot)) {
                    $path = explode('/', $site->documentRoot);
                    if (!empty($path[2]))
                        $site->linuxUser = $path[2];
                }

                $arSites[] = $site;
            }
        }
        return $arSites;
    }

    /**
     * Рассчет что установлено и работает через Postfix
     * @param Site $site
     * @return string
     */
    static function getMailSender(Site $site) {
        if (file_exists($site->httpdConfigFile)) {
            $confFile = file_get_contents($site->httpdConfigFile);
            $re = '/(sendmail.postfix)[^"\']*/';
            preg_match_all($re, $confFile, $matches, PREG_SET_ORDER, 0);
            $sender = current(current($matches));
            $sender = str_replace(['sendmail.postfix', '-i -t -f'], '', $sender);
            $sender = trim($sender);
            $site->mailSender = $sender;
        }

        return $site->mailSender;
    }

    static function setSitesPath($path)
    {
        if (is_dir(realpath($path))) {
            self::$pathSites = $path;
        }
    }

    static function rollBack(Site $site)
    {
        $fileBack = self::$pathSaveConfigs . '/' . date('Y.m.d.') . basename($site->httpdConfigFile);
        copy($fileBack, $site->httpdConfigFile);
    }

    /**
     * @param Site   $site
     * @param string $newDir
     * @return bool
     */
    static function setSessionDir(Site $site, $newDir)
    {
        if (!self::saveConfig($site->httpdConfigFile)) return false;

        $data = file_get_contents($site->httpdConfigFile);

        $re   = '/(session.save_path).*/';
        $data = preg_replace($re, "session.save_path {$newDir}", $data);

        if (!file_put_contents($site->httpdConfigFile, $data)) {
            if (class_exists(Log::class)) Log::setError('Не удалось сохранить конфиги с изменением сесси сайта ' . $site->id);
            return false;
        }

        return true;
    }

    /**
     * @param Site   $site
     * @param string $newDir
     * @return bool
     */
    static function setUploadDir(Site $site, $newDir)
    {
        if (!self::saveConfig($site->httpdConfigFile)) return false;
        $data = file_get_contents($site->httpdConfigFile);

        $re   = '/(upload_tmp_dir).*/';
        $data = preg_replace($re, "upload_tmp_dir {$newDir}", $data);

        if (!file_put_contents($site->httpdConfigFile, $data)) {
            if (class_exists(Log::class)) Log::setError('Не удалось сохранить конфиги с изменением директории для загрузок файлов сайта ' . $site->id);
            return false;
        }

        return true;
    }

    /**
     * Проверяет общее состояние сайта (краткий вариант)
     * @param Site $site
     * @return bool
     */
    static function status(Site $site) {
        if (!is_dir($site->documentRoot)
        || !file_exists($site->httpdConfigFile)
        || !is_dir($site->dirUploads)
        || !is_dir($site->dirSessions)
        ) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Развернутое состояние состояния настроек
     * @param Site $site
     * @return array
     */
    static function getArStatus(Site $site) {
        $site->httpd['rootDir'] = self::getRoot($site->httpdConfigFile);
        return [
            'documentRoot' => is_dir($site->documentRoot),
            'httpdConfigFile' => file_exists($site->httpdConfigFile),
            'dirUploads' => is_dir($site->dirUploads),
            'dirSessions' => is_dir($site->dirSessions),
            'rootDir' => $site->documentRoot == $site->httpd['rootDir']
        ];
    }

    static function getDocumentRoot(Site $site) {
        if (!file_exists($site->httpdConfigFile)) return false;
        $data = file_get_contents($site->httpdConfigFile);
        $re = '/(DocumentRoot ).*/';
        preg_match_all($re, $data, $matches, PREG_SET_ORDER, 0);
        $getInfo = current(current($matches));
        $getInfo = str_replace('DocumentRoot ', '', $getInfo);
        $getInfo = trim($getInfo);
        return $getInfo;
    }

    static function getSessionsDir(Site $site) {
        if (!file_exists($site->httpdConfigFile)) return false;
        $data = file_get_contents($site->httpdConfigFile);
        $re = '/(session.save_path ).*/';
        preg_match_all($re, $data, $matches, PREG_SET_ORDER, 0);
        $getInfo = current(current($matches));
        $getInfo = str_replace('session.save_path ', '', $getInfo);
        $getInfo = trim($getInfo);
        return $getInfo;
    }

    static function getUploadsDir(Site $site) {
        if (!file_exists($site->httpdConfigFile)) return false;
        $data = file_get_contents($site->httpdConfigFile);
        $re = '/(upload_tmp_dir ).*/';
        preg_match_all($re, $data, $matches, PREG_SET_ORDER, 0);
        $getInfo = current(current($matches));
        $getInfo = str_replace('upload_tmp_dir ', '', $getInfo);
        $getInfo = trim($getInfo);
        return $getInfo;
    }

    static function restart() {
        $info = Server::getServerInfo();
        if ($info['release'] > 6) {
            exec('systemctl restart httpd');
        } else {
            exec('service httpd restart');
        }
    }

    static function getRoot($file) {
        if (!file_exists($file)) return;
        $data = file_get_contents($file);

        $reg = '/^(?!.*[#$]).*DocumentRoot[\t "]+(.+[a-zA-Z0-9])/m';
        preg_match_all($reg, $data, $matches, PREG_SET_ORDER, 0);
        $result = current($matches);
        $result = $result[1];
        return $result;
    }

    static function setMailSender(Site $site, $name) {
        /*
        if (!self::saveConfig($site->httpdConfigFile) || empty($name)) return false;
        d($site);
        $data = file_get_contents($site->httpdConfigFile);


        $re   = '/(sendmail_path).* /';
        $data = preg_replace($re, "sendmail_path {$newDir}", $data);

        if (!file_put_contents($site->httpdConfigFile, $data)) {
            if (class_exists(Loger::class)) Loger::setError('Не удалось сохранить конфиги с изменением директории для загрузок файлов сайта ' . $site->id);
            return false;
        }
        */

        // php_admin_value sendmail_path "/usr/sbin/sendmail.postfix -i -t -f licvidator.ua"
    }

    static function deleteHttpdConfFile(Site $site) {
        if (!empty($site->httpdConfigFile)
            && file_exists($site->httpdConfigFile)
            && unlink($site->httpdConfigFile)
        ) {
            return true;
        }

        return false;
    }

    static function disableSite(Site $site) {
        Log::setLog("Method disableSite({$site->id})");
        if (file_exists($site->httpdConfigFile)) {
            $offFile = self::getConfigPathDefault($site->id, false);
            $comand = "mv {$site->httpdConfigFile} {$offFile}";
            exec($comand);
            Log::setLog($comand);
        } else {
            Log::setError("Нету файла для отключения сайта({$site->id}) file:{$site->httpdConfigFile}");
        }
    }

    static function activeteSite(Site $site) {
        if ($site->status) return true;
        Log::setLog("Method activeteSite({$site->id})");
        if (file_exists($site->httpdConfigFile)) {
            $activeFile = self::getConfigPathDefault($site->id);
            $comand = "mv {$site->httpdConfigFile} {$activeFile}";
            exec($comand);
            Log::setLog($comand);
        } else {
            Log::setError("Нету файла для отключения сайта({$site->id}) file:{$site->httpdConfigFile}");
        }
    }
}