<?php

namespace Pachverk;

class Rest
{
    /** @var string Путь куда будут кидаться запросы */
    public $path = '';
    /** @var array Массива параметров который будет отправлен на нужную страницу */
    public $arParams = [];
    /** @var string post|get Метод отправки данных */
    public $method;
    /** @var string Порт на который отправлять все запросы, пока только для поста используется */
    public $port;
    public $timeout = 0;
    /**
     * TODO возможно нужно будет изменить
     * @var string json|null В каком виде прилетят данные и нужно их обработать
     */
    public $dataType;
    /**
     * TODO Нужно будет удалить временное
     * @var bool
     */
    public $addGetToPost;
    /** @var string json|bull В каком формате будут отправлены данные */
    public $contenType;


    private $response;
    private $dataCript;

    /**
     * @var array Формат в котором отправлять запросы <ul>
     *            <li> Content-Type: application/json
     *            <li> 'Content-Length: ' . strlen($arFields)
     */
    private $arHeaders;
    private $arSetHeaders=[];
    /** @var int Время принудительного отключения скрипты */
    private $lastRespons;

    /**
     * Отправка запроса
     */
    public function get()
    {
        if (empty($this->path)) return;

        $ch = curl_init();

        // Преобразование параметров
        if ($this->contenType === 'json') {
            $this->arParams = json_encode($this->arParams);
        }

        // подготовка контента
        $path = $this->path;
        if ($this->method == 'post') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->arParams);
            // TODO Нужно убрать этот кусок
            if ($this->addGetToPost) $path .= '?'.http_build_query($this->arParams);
        } else {
            if (!empty($this->arParams)) {
                $path .= '?'.http_build_query($this->arParams);
            }
        }

        // Ограничение по времени
        if ($this->timeout > 0) {
            curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        }

        // Добавить порт для запроса
        if ($this->port) {
            curl_setopt($ch, CURLOPT_PORT, $this->port);
        }

        // Заголовки
        if ($this->contenType === 'json') {
            $this->arSetHeaders = $this->arHeaders;
            $this->arSetHeaders[] = 'Content-Type: application/json';
            $this->arSetHeaders[] = 'Content-Length: ' . strlen($this->arParams);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arSetHeaders);
        }

        // Trash
        if (0) {
            // if ($this->methos === 'post') {
            // \curl_setopt($ch, CURLOPT_POST, 1);
            // \curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            // $this->debugCurl['CURLOPT_POST'] = 1;
            // }

            // if ($this->contenType === 'application/json') {
            //     $arFields = json_encode($this->arParams);
            //     $curlHeaders = [
            //         'Content-Type: application/json',
            //         'Content-Length: ' . strlen($arFields)
            //     ];
            //     \curl_setopt($ch, CURLOPT_POSTFIELDS, $arFields);
            //     \curl_setopt($ch, CURLOPT_HTTPHEADER, $curlHeaders);
            //     $this->debugCurl['CURLOPT_HTTPHEADER'] = $curlHeaders;
            //
            // }

            // if (!empty($this->port)) {
            //     \curl_setopt($ch, CURLOPT_PORT, $this->port);
            //     $this->debugCurl['CURLOPT_PORT'] = $this->port;
            // }
            // $arFields = json_encode($this->arParams);
            // $curlHeaders = [
            //     'Content-Type: application/json',
            //     'Content-Length: ' . strlen($arFields)
            // ];
            // curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, $this->arParams);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $path);
        $this->response = $this->lastRespons = curl_exec($ch);
        curl_close($ch);

        // switch ($this->dataCript) {
        //     case 'base':
        //         $this->lastRespons = base64_decode($this->lastRespons);
        //         break;
        // }

        switch ($this->dataType) {
            case 'json':
                $this->lastRespons = json_decode($this->lastRespons);
                break;
            case 'serialize':
                $this->lastRespons = unserialize($this->lastRespons);
                break;
        }

        return $this->lastRespons;
    }

    /**
     * @param string $type base|none
     * @return $this
     */
    public function dataCript($type) {
        $this->dataCript = $type;
        return $this;
    }

    /**
     * В каком виде лежит информация на противоположной стороне
     * serialize|json
     * @param string $dataType
     * @return Rest
     */
    public function dataType($dataType) {
        $this->dataType = $dataType;
        return $this;
    }

    /**
     * Параметры которые должны будут улететь с запросом
     * @param array $arParams
     * @return Rest
     */
    public function params(array $arParams) {
        $this->arParams = $arParams;
        return $this;
    }

    public function getDebugInfo()
    {
        return $this->debugCurl;
    }

    /**
     * post|get
     * @return $this
     */
    public function method($method) {
        $this->method = $method;
        return $this;
    }

    /**
     * @param string $path
     * @return Rest
     */
    public static function request($path) {
        $ob = new self;
        $ob->path = $path;
        return $ob;
    }
}