<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 14.08.2019
 * Time: 15:46
 */

namespace Pachverk;


use Bitrix\Main\UI\Uploader\Log;

class HotFixer
{
    /**
     * Отвязывает модули от общей директории
     */
    static function changeLinksToDir() {
        /**
         * @var Site $site
         */
        $sites = Httpd::getSiteList();
        foreach ($sites as $site) {
            if (!empty($site->documentRoot)
            && is_dir($site->documentRoot.'/bitrix')
            && is_dir($site->documentRoot.'/bitrix/modules')
            ) {
                if (is_link($site->documentRoot.'/bitrix')) continue;

                $dir = $site->documentRoot.'/bitrix/modules';
                $iterator = new \DirectoryIterator($dir);
                foreach ($iterator as $fileinfo) {
                    if ($fileinfo->isDir()) {
                        if ($fileinfo->isLink()) {
                            $moduleDir = "$dir/{$fileinfo->getFilename()}";
                            $to = $fileinfo->getRealPath();

                            unlink($moduleDir);
                            $site->comands[] =$comand = "cp -r $to ".dirname($moduleDir);
                            exec($comand);
                            exec("chown -R {$site->linuxUser}:{$site->linuxUser} {$moduleDir}");
                            $stop = true;
                        }
                    }
                }

                if ($stop) {
                    d($site);
                    die();
                }
            }
        }
    }

    static function closeModules() {
        /**
         * @var Site $site
         */
        $findModules = [
            "yamasters.comments",
            "yamasters.core",
            "yamasters.lighthorse",
            "yamasters.light.horse",
            "yamasters.develop",
            "yamasters.luckystars",
            "yamasters.luckystarl",
            "yamasters.luckystar",
            "yamasters.luckystarm",
            "yamasters.luckystarxxl",
        ];

        $sites = Httpd::getSiteList();
        foreach ($sites as $site) {
            $moduleDir = "{$site->documentRoot}/bitrix/modules";
            if (is_dir($moduleDir)) {
                $dir = new \DirectoryIterator($moduleDir);
                foreach ($dir as $file) {
                    if (in_array($file->getFilename(), $findModules)
                    && strpos($file->getFilename(), 'yamasters') !== false
                    ) {
                        Tools::closeDir($file->getRealPath());
                        Log::setLog("Dir closed {$file->getRealPath()}");
                    }
                }
            }
        }

    }
    
    static function changeDirsFromSiteToSites() {
        $arParams = [
            'restart'               => false,
            'changeSiteDir'         => false,
            'changeWithLinuxUsers'  => false,
            'changeSiteInBitrixDir' => false,
            'changeSiteWithLinks'   => false,
        ];


        /**
         * @var Site $site
         */
        $sites = Httpd::getSiteList();
        foreach ($sites as $site) {
            $oldDir = "/home/{$site->linuxUser}/site";
            $newDir = "/home/{$site->linuxUser}/sites";

            // Абсолютно чистая ситуация
            if (1 && is_dir($oldDir) && !is_dir($newDir)) {
                if ($arParams['changeSiteDir']) {
                    if (count(scandir($oldDir)) == 3) {

                        $comand = "mv $oldDir $newDir";
                        exec($comand);
                        Log::setLog($comand);
                        $site->documentRoot = "$newDir/{$site->id}";

                        if ($site->update()) {
                            Log::setSuccess("[$site->id] Успешно обновлен, домены для проверки: ". implode(', ', $site->domains));
                        }
                    } else {
                        d('Нужно проверить директорию на наличие других сайтов', $site);
                    }
                }
            }

            // Обновление директорий с уже имеющимся пользователем в линухе
            if ($site->documentRoot !== $site->getDefaultDocumentRoot()
            && !empty($site->linuxUser)
            && $site->linuxUser !== 'bitrix'
            ) {
                if ($arParams['changeWithLinuxUsers']) {
                    $oldDir = $site->documentRoot;
                    $newDir = $site->getDefaultDocumentRoot();
                    mkdir($site->getDefaultSitesPath(), 0755);
                    chown($site->getDefaultSitesPath(), $site->linuxUser);

                    $comand = "mv $oldDir $newDir";
                    exec($comand);
                    Log::setLog($comand);
                    $site->documentRoot = $site->getDefaultDocumentRoot();

                    if ($site->update()) {
                        Log::setSuccess("[$site->id] Успешно обновлен, домены для проверки: ". implode(', ', $site->domains));
                    }
                }
            }

            if ($site->documentRoot !== $site->getDefaultDocumentRoot()
            && !empty($site->linuxUser)
            && $site->linuxUser === 'bitrix'
            ) {
                if ($arParams['changeSiteInBitrixDir']) {
                    if (!is_link($site->documentRoot.'/bitrix')
                    && !is_link($site->documentRoot.'/upload')
                    && !is_link($site->documentRoot.'/images')
                    ) {
                        $oldDirRoot = $site->documentRoot;
                        $site->linuxUser = Site::getDefaultUsetNameByID($site->id);
                        $site->documentRoot = $site->getDefaultDocumentRoot();

                        $site->modifierDomains();
                        if ($site->createSpace()) {
                            $comand = "mv $oldDirRoot ".$site->getDefaultSitesPath();
                            Log::setLog($comand);
                            exec($comand);
                            exec("chown -R {$site->linuxUser}:{$site->linuxUser} ".$site->getDefaultDocumentRoot());

                            $phpIni = $site->getDefaultDocumentRoot().'/bitrix/php_interface/init.php';
                            if (file_exists($phpIni)) {
                                $data = file_get_contents($phpIni);
                                $data = str_replace('<?require \'/home/bitrix/www/bitrix/lib/antispam.php\';?>', '', $data);
                                file_put_contents($phpIni, $data);
                            }
                        }

                        if (file_exists("/etc/nginx/bx/site_avaliable/bx_ext_{$site->id}.conf")) {
                            unlink("/etc/nginx/bx/site_avaliable/bx_ext_{$site->id}.conf");
                            unlink("/etc/nginx/bx/site_enabled/bx_ext_{$site->id}.conf");
                        }

                        if (file_exists("/etc/nginx/bx/site_avaliable/bx_ext_ssl_{$site->id}.conf")) {
                            unlink("/etc/nginx/bx/site_avaliable/bx_ext_ssl_{$site->id}.conf");
                            unlink("/etc/nginx/bx/site_enabled/bx_ext_ssl_{$site->id}.conf");
                        }

                        $arParams['restart'] = true;
                    } else {
                        Log::setLog("Site with lincks {$site->id}");
                        continue;
                    }
                }
            }

            if ($arParams['changeSiteWithLinks']
            && $site->linuxUser === 'bitrix'
            && (is_link($site->documentRoot.'/bitrix')
                || is_link($site->documentRoot.'/upload')
                || is_link($site->documentRoot.'/images')
                  )) {
                if (is_link($site->documentRoot.'/bitrix')) {
                    $path = readlink($site->documentRoot.'/bitrix');
                    $id = str_replace(['/home/bitrix/ext_www/', '/bitrix'], '', $path);


                    if (!empty($id)) {
                        $mainSite = Site::getSiteByID($id);
                    } else $mainSite = null;

                    if (is_object($mainSite) && !empty($mainSite->linuxUser) && $mainSite->linuxUser !== 'bitrix') {
                        $oldDir = $site->documentRoot;
                        $site->linuxUser = $mainSite->linuxUser;
                        $site->documentRoot = $site->getDefaultDocumentRoot();
                        $comand = "mv $oldDir ".$site->getDefaultSitesPath();
                        exec($comand);
                        Log::setLog($comand);

                        if (file_exists("/etc/nginx/bx/site_avaliable/bx_ext_{$site->id}.conf")) {
                            unlink("/etc/nginx/bx/site_avaliable/bx_ext_{$site->id}.conf");
                            unlink("/etc/nginx/bx/site_enabled/bx_ext_{$site->id}.conf");
                        }

                        if (file_exists("/etc/nginx/bx/site_avaliable/bx_ext_ssl_{$site->id}.conf")) {
                            unlink("/etc/nginx/bx/site_avaliable/bx_ext_ssl_{$site->id}.conf");
                            unlink("/etc/nginx/bx/site_enabled/bx_ext_ssl_{$site->id}.conf");
                        }

                        $site->modifierDomains();

                        if ($site->update()) {
                            Log::setSuccess("[$site->id] Успешно обновлен, домены для проверки: ". implode(', ', $site->domains));
                        }

                        unlink($site->documentRoot.'/bitrix');
                        symlink($mainSite->documentRoot.'/bitrix', $site->documentRoot.'/bitrix');

                        if (is_link($site->documentRoot.'/upload')) {
                            unlink($site->documentRoot.'/upload');
                            symlink($mainSite->documentRoot.'/upload', $site->documentRoot.'/upload');
                        }

                        if (is_link($site->documentRoot.'/images')) {
                            unlink($site->documentRoot.'/images');
                            symlink($mainSite->documentRoot.'/images', $site->documentRoot.'/images');
                        }

                        exec("chown -R {$site->linuxUser}:{$site->linuxUser} {$site->documentRoot}");

                        $arParams['restart'] = true;
                    }


                }
            }
        }

        if ($arParams['restart']) {
            Httpd::restart();
            Nginx::restart();
        }
    }

    /**
     * Тут будет попытка охватить фикс всех возможных ошибок
     */
    static function fixAllProblems() {
        /**
         * @var Site $site
         */
        $needRestartServer      = false;
        $needChangeLinks        = false; // Переобразованиелинканутых модулей
        $needBreakInForeach     = false; // Сделает обрывание что бы дальше не пошла цепочка (после натыкания на первую проблему)
        $needFixSessions        = true;
        $needFixUploads        = true;
        $changePermissions      = true; // Определение прав по умолчанию для важных директорий
        $changeDirWithLinuxUser = true; // Обработка всех пользователей с уже имеющимися домашними директориями
        $changeDomains          = true; // Позволит добавить недостающие домены
        $nginxFix               = true; // Пока только удаление старых файлов + Создаст настройки если их нет из конфигов объекта

        // Серезные телодвижения которые требуют контроля
        if (1) {
            foreach (Site::getSitesWithProblem() as $site) {
                $problem = false;
                $needBreak = false;

                if (1) {
                    if (!empty($site->linuxUser)
                        && $site->linuxUser != 'bitrix'
                    ) {
                        if ($changeDirWithLinuxUser
                        && is_dir($site->documentRoot)
                        && $site->documentRoot !== $site->getDefaultDocumentRoot()
                        ) {
                            if (!is_dir($site->getDefaultSitesPath())) {
                                mkdir($site->getDefaultSitesPath(), 0755, true);
                                Log::setLog('mkdir ('.$site->getDefaultSitesPath().')');
                            }

                            $comand = 'mv '.$site->documentRoot.' '.$site->getDefaultDocumentRoot();
                            exec($comand);
                            Log::setLog($comand);

                            if (is_dir($site->getDefaultDocumentRoot())) {
                                $site->documentRoot = $site->getDefaultDocumentRoot();
                                $site->modifierDomains();
                                if ($site->update()) {
                                    Log::setSuccess("[$site->id] Сайт успешно обновлен, домены для проверки: ". implode(', ', $site->domains));
                                    $needRestartServer = true;
                                }
                            }

                            $problem = true;
                        }

                        $needBreak = true;
                    }
                }

                if ($needBreakInForeach) {
                    if ($needBreak) {
                        break;
                    }
                }

                if ($problem) {
                    d($site);
                }
            }
        }

        // Шлифовачные настройки сайтов
        if (1) {
            $sites = Site::getSites();
            foreach ($sites as $site) {
                $problem = false;
                // if ($site->validate() === true && $site->checkNginxFiles()) continue;

                $needBreak = false;
                $needUpdate = false;

                // Обработка линков
                /*if ($needChangeLinks) {
                    foreach (new \DirectoryIterator($dir['modules']) as $fileInfo) {
                        if ($fileInfo->isLink()) {
                            if (in_array($fileInfo->getBasename(), ['yamasters.develop'])) {
                                Loger::setLog("Unlink ".$fileInfo->getBasename());
                                unlink($fileInfo->getPath().'/'.$fileInfo->getBasename());
                            } else {
                                $target = $fileInfo->getLinkTarget();
                                $dir = $fileInfo->getPath();
                                $file = $fileInfo->getPath().'/'.$fileInfo->getBasename();
                                if (file_exists($target) && is_dir($file) && is_link($file)) {
                                    Loger::setLog("Unlink ".$fileInfo->getBasename());
                                    unlink($file);

                                    $comand = "cp -r $target $dir";
                                    Loger::setLog($comand);
                                    exec($comand);

                                    $comand = "chown -R {$site->linuxUser}:{$site->linuxUser} {$file}";
                                    Loger::setLog($comand);
                                    exec($comand);
                                }
                            }

                            if ($needBreakInForeach) {
                                $needBreak = true;
                            }
                        }
                    }
                }*/

                // Изменение базовых прав
                if ($changePermissions && !in_array(realpath($site->getSpacePath()), ['/home', '/'])) {
                    chmod($site->getSpacePath(), 0700);
                    chown($site->getSpacePath(), $site->linuxUser);
                    chgrp($site->getSpacePath(), $site->linuxUser);

                    chmod($site->getDefaultSitesPath(), 0755);
                    chown($site->getDefaultSitesPath(), $site->linuxUser);
                    chgrp($site->getDefaultSitesPath(), $site->linuxUser);

                    chmod($site->getDefaultDocumentRoot(), 0755);
                    chown($site->getDefaultDocumentRoot(), $site->linuxUser);
                    chgrp($site->getDefaultDocumentRoot(), $site->linuxUser);
                }

                // Фикс nginx файлов
                if ($nginxFix) {
                    $nginxOldFile = "/etc/nginx/bx/site_avaliable/bx_ext_ssl_{$site->id}.conf";
                    $nginxSslOldFile = "/etc/nginx/bx/site_avaliable/bx_ext_ssl_{$site->id}.conf";
                    if (file_exists($nginxOldFile)) {
                        unlink($nginxOldFile);
                        unlink($nginxSslOldFile);
                        $needUpdate = true;
                        $needBreak = true;
                    }
                }

                // Создать конфиг файл
                if ($nginxFix) {
                    if ($site->checkNginxFiles() === false) {
                        $needUpdate = true;
                    }
                }

                // Проверка есть ли у сайта сертификат и попытка его вкрутить
                if ($nginxFix) {
                    $cert = Nginx::getPathSslFile($site->id);

                    if ($site->checkNginxFiles()
                        && file_exists($cert)
                        && file_exists($site->nginxConfigFile)
                        && strpos(file_get_contents($site->nginxConfigFile), '443') === false
                    ) {
                        $needUpdate = true;
                    }
                }

                if ($changeDomains && $site->modifierDomains()) {
                    $needUpdate = true;
                    if ($needBreakInForeach) {
                        $needBreak = true;
                    }
                }

                if ($needFixSessions && !is_dir($site->getSessionsDir())) {
                    mkdir($site->getSessionsDir(), 0755);
                    chown($site->getSessionsDir(), $site->linuxUser);
                    if (!empty($site->getSessionsDir()) && is_dir($site->getSessionsDir())) {
                        Log::setSuccess("[{$site->id}] Успешно создана директория для сессий ".$site->getSessionsDir());
                    } else {
                        Log::setError("[{$site->id}] Не удалось создать директорию для сессий ".$site->getSessionsDir());
                    }
                }

                if ($needFixUploads && !is_dir($site->getUploadsDir())) {
                    mkdir($site->getUploadsDir(), 0755);
                    chown($site->getUploadsDir(), $site->linuxUser);
                    if (!empty($site->getUploadsDir()) && is_dir($site->getUploadsDir())) {
                        Log::setSuccess("[{$site->id}] Успешно создана директория для загрузок документов ".$site->getUploadsDir());
                    } else {
                        Log::setError("[{$site->id}] Не удалось создать директорию для загрузок документов ".$site->getUploadsDir());
                    }
                }
                /*


                if (1) {
                    $file = $site->documentRoot.'/bitrix/php_interface/after_connect.php';
                    if (file_exists($file)) {
                        $data = file_get_contents($file);
                        if (strpos($data, 'cp1251') !== false && in_array($site->encoding , ['utf8', 'utf-8'])) {
                            Loger::setLog("[{$site->id}] Изменилась кодировка для сайта на 1251");
                            $site->encoding = 'windows-1251';
                            $needUpdate = true;
                        }
                    }
                }

                if (1) {
                    $oldFileNginx = "/etc/nginx/bx/site_avaliable/bx_ext_{$site->id}.conf";
                    $oldFileNginxSsl = "/etc/nginx/bx/site_avaliable/bx_ext_ssl_{$site->id}.conf";
                    $oldFileNginxLinck = "/etc/nginx/bx/site_enabled/bx_ext_{$site->id}.conf";
                    $oldFileNginxSslLink = "/etc/nginx/bx/site_avaliable/bx_ext_ssl_{$site->id}.conf";

                    if (file_exists($oldFileNginx)) {
                        Loger::setLog("unlink $oldFileNginx");
                        unlink($oldFileNginx);
                        Loger::setLog("unlink $oldFileNginxLinck");
                        unlink($oldFileNginxLinck);
                        $needUpdate = true;
                    }

                    if (file_exists($oldFileNginxSsl)) {
                        Loger::setLog("unlink $oldFileNginxSsl");
                        unlink($oldFileNginxSsl);
                        Loger::setLog("unlink $oldFileNginxSslLink");
                        unlink($oldFileNginxSslLink);
                        $needUpdate = true;
                    }

                }

                // Проверка включенности сертификатов
                if (1) {
                    $certPath = "/etc/nginx/ssl/{$site->id}.pem";
                    if (file_exists($certPath)) {
                        if ($site->checkNginxFiles()) {
                            $nginxInfo = file_get_contents($site->nginxConfigFile);
                            if (strpos($nginxInfo, $certPath) === false) {
                                Loger::setLog("[{$site->id}] Нашелся сертификат сайта но не подключен к сайту, pапущен процесс пересборки настроек");
                                $needUpdate = true;
                            }
                        }
                    }
                }
                */

                if ($needUpdate) {
                    $problem = true;
                    $site->update();
                    $needRestartServer = true;
                }

                if ($needBreakInForeach) {
                    if ($needBreak) {
                        break;
                    }
                }

                if ($problem) {
                    d($site);
                }
            }
        }

        if ($needRestartServer) {
            Httpd::restart();
            Nginx::restart();
        }
    }

    static function fixHttpd() {
        /** @var Site $site */
        $sites = Httpd::getSiteList();
        foreach ($sites as $site) {
            $fix = false;
            foreach ($site->domains as $domain) {
                if (strpos($domain, 'ServerAdmin') !== false) {
                    $fix = true;
                    break;
                }
            }

            if ($fix) {
                $data = file_get_contents($site->httpdConfigFile);
                $data = str_replace('ServerAdmin', "\nServerAdmin", $data);
                file_put_contents($site->httpdConfigFile, $data);
            }
        }
    }

    /**
     * Если место на фтп забилось и нужно срочно подчистить
     * сделает принудительную очистку до нужного состояния относительно сайтам которые нашло
     */
    static function clearFTPServer() {
        /** @var Site $site */
        $sites = Site::getSites();
        $hostName = shell_exec('hostname');
        $hostName = trim($hostName);
        $ftp = FTP::ini();
        foreach ($sites as $site) {
            if ($ftp) {
                $ftp->deleteOldFiles('/'.$hostName.'/'.$site->id.'/');
            }
        }
        if ($ftp) $ftp->closeConnect();
    }
}