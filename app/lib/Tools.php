<?php


namespace Pachverk;


class Tools
{
    /**
     * Сгенерировать случайный параль
     * @param integer $number    Длина пароля
     * @param boolean $difficult Добавить ли усложненные значения к паролю
     * @return string
     */
    static function generatePassword ($number=12, $difficult=false) {
        $arr = array(
            'a','b','c','d','e','f',
            'g','h','k','m','n','p',
            'r','s','t','u','v','x',
            'y','z','A','C','E','F',
            'G','H','K','M','N','P',
            'R','S','T','U','V','X',
            'Y','Z','2','3','4','5',
            '6','7','8','9',
            "@", "*", "-", "+", "!",
            ",", ".", ";", ":", "{",
            "}", "(", ")", "%",
        );

        // Генерируем пароль
        $pass = "";
        for($i = 0; $i < $number; $i++) {
            // Вычисляем случайный индекс массива
            $index = rand(0, count($arr) - 1);
            $pass .= $arr[$index];
        }

        if ($difficult) {
            // Обрежем последние 9 символов
            if (strlen($pass) > 9)
                $pass = substr($pass, 0, 9);

            $pass = $pass.'-_^#,+7zA'; // Добавим новые 9 сложных символов, и две цифры
            $pass = str_shuffle($pass); // Перемещаем строку
        }
        return $pass;
    }

    /**
     * Подсчитает место у конкретной директории (bites)
     * @param string $dir Путь к директории
     * @return int
     */
    static function getDirSize ($dir = '.') {
        /**
         * @var \SplFileInfo $info
         */
        $size = 0;
        $rDir = new \RecursiveDirectoryIterator($dir);
        $iterator = new \RecursiveIteratorIterator($rDir);
        foreach ($iterator as $info) {
            // if ($info->isFile()) echo $info->getFilename().PHP_EOL;
            // if ($info->isFile())
                $size += $info->getSize();
        }
        return $size;
    }

    static function dataSize($Bytes) {
        $Type=array("k", "m", "g", "t");
        $counter=0;

        $arResult = array();
        $arResult["b"] = round($Bytes, 1);
        while($Bytes>=1024) {
            $Bytes/=1024;
            $arResult[$Type[$counter]."b"] = round($Bytes, 1);
            $counter++;
        }

        if ($arResult["mb"] > 0 && !$arResult["gb"]) {
            $arResult["gb"] = round($Bytes/=1024, 1);
        }

        return $arResult;
    }

    static function dataSizeFull ($Bytes) {
        $Type= ["", "K", "M", "G", "T"];
        $counter = 0;
        while($Bytes >= 1024) {
            $Bytes /= 1024;
            $counter++;
        }

        $size = round($Bytes, 1);
        $b = $Type[$counter]."b";
        return array(
            "size" => $size,
            "b" => $b
        );
    }

    static function closeDir($path) {
        exec("chown -R root:root {$path}");
        exec("find {$path} -type d -exec chmod 0711 {} \;");
        exec("find {$path} -type f -exec chmod 0604 {} \;");
    }

    /**
     * Поиск будет работать от начала фразы и до конца строки
     * возмет самое первое совпадение
     * @param $data
     * @param $word
     * @return bool
     */
    static function getStringByWord($data, $word) {
        $re = "/($word).*/";
        preg_match_all($re, $data, $matches, PREG_SET_ORDER, 0);
        $value = current(current($matches));
        $value = str_replace($word, '', $value);
        $value = trim($value);
        return $value;
    }

    static function mkdir($path, $mode, $recursive = false, $owner = false) {
        if (!is_dir($path)) {
            mkdir($path, $mode, $recursive);
        }

        if (is_dir($path) && !empty($owner)) {
            chown($path, $owner);
            chgrp($path, $owner);
        }
    }
}