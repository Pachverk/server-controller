<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 06.08.2019
 * Time: 14:24
 */

namespace Pachverk;


class Server
{
    protected $hostName;
    protected $phpVersion;

    /**
     * Bнформация о сервере
     * @return array OS, release, fileInfo
     */
    static function getServerInfo() {
        static $result;
        if (isset($result)) return $result;

        $result = [];

        // Centos
        if (file_exists('/etc/redhat-release')) {
            $result['fileInfo'] = $infoFile = trim(file_get_contents('/etc/redhat-release'));
            $infoFile = explode(' ', $infoFile);
            $result['OS'] = $infoFile[0];
            $result['release'] = round($infoFile[3], 2);
        }

        return $result;
    }
}