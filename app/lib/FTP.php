<?php


namespace Pachverk;


class FTP
{
    private $login;
    private $password;
    private $server;
    private $connectID;
    private $connectStatus;

    static function ini() {
        static $ob;
        if (!isset($ob)) {
            $ob = new FTP();
            $ob->login = Settings::getOption('ftpLogin');
            $ob->password = Settings::getOption('ftpPassWord');
            $ob->server = Settings::getOption('ftpServerName');
            $ob->connectID = ftp_connect($ob->server);
            if ($ob->connectID) {
                $ob->connectStatus = ftp_login($ob->connectID, $ob->login, $ob->password);
            } else {
                Log::setError("Не удалось установить соединение с сервером '{$ob->server}'");
            }
        }
        return ($ob->connectID > 0 && $ob->connectStatus) ? $ob : null;
    }

    public function closeConnect() {
        ftp_close($this->connectID);
    }

    public function deleteOldFiles($path, $count=7) {
        $list = ftp_nlist($this->connectID, $path);
        $listForSort = [];
        if (count($list) > $count) {

            foreach ($list as $item) {
                $time = ftp_mdtm($this->connectID, $item);
                $listForSort[$item] = $time;
            }
            arsort($listForSort);

            $itetator = 0;
            foreach ($listForSort as $file => $time) {
                if (++$itetator > $count) {
                    if (ftp_delete($this->connectID, $file)) {
                        Log::setSuccess("File is deleted $file");
                    } else {
                        Log::setError("File is fail deleted $file");
                    }
                }
            }
        }
    }

    static function testConnect() {
        if (!function_exists('ftp_connect')) {
            return false;
        }

        $conn_id = ftp_connect(Settings::getOption('ftpServerName'));
        if (!ftp_login($conn_id, Settings::getOption('ftpLogin'), Settings::getOption('ftpPassWord'))) {
            return false;
        }
        ftp_close($conn_id);
        return true;
    }

    static function createDir($path) {
        $conn_id = ftp_connect(Settings::getOption('ftpServerName'));
        if (!ftp_login($conn_id, Settings::getOption('ftpLogin'), Settings::getOption('ftpPassWord'))) {
            if (class_exists(Log::class))
                Log::setError("Не удалось установить ftp соединение");
            return false;
        }
        ftp_pasv($conn_id, true);
        ftp_chdir($conn_id, '/');

        foreach (explode('/', $path) as $dirName) {
            if (!empty($dirName)) {
                $list = ftp_nlist($conn_id, '.');

                if (in_array($dirName, $list)) {
                    ftp_chdir($conn_id, $dirName);
                    continue;
                }

                if(!ftp_mkdir($conn_id, $dirName)) {
                    ftp_close($conn_id);
                    if (class_exists(Log::class)) Log::setError("Не удалось создать директорию {$dirName}");
                    return false;
                } else {
                    ftp_chdir($conn_id, $dirName);
                }
            }
        }
        ftp_close($conn_id);
        return true;
    }

    static function getSiteInfo($hostName, $siteID) {
        if (empty($hostName)) {
            Log::setError('Не указан хост для получения информации');
            return false;
        }

        if (empty($siteID)) {
            Log::setError('Не указан siteID по которому нужно будет доставать информацию');
            return false;
        }

    }

    /**
     * @param      $file
     * @param      $fullPath
     * @param bool $delFileAfterPut
     * @return bool
     */
    static function sftpPutFile($file, $fullPath, $delFileAfterPut=true) {
        if (file_exists($file)) {
            if (FTP::createDir(dirname($fullPath))) {
                $fileName = basename($file);
                $password = Settings::getOption('ftpPassWord');
                $login = Settings::getOption('ftpLogin');
                $server = Settings::getOption('ftpServerName');
                $comand = "sshpass -p \"$password\" scp $file {$login}@{$server}:$fullPath";
                $log = shell_exec($comand);
                if (!empty($log)) {
                    Log::setError($log);
                } else {
                    Log::setSuccess("Успешно заброшен архив ($fileName) в хранилище $fullPath");
                }

                if ($delFileAfterPut) {
                    unlink($file);

                    if (file_exists($file)) {
                        Log::setLog("Не удалось корретно удалить файл $file");

                        $comand = "rm -f $file";
                        Log::setLog($comand);
                        exec($comand);

                        if (file_exists($file)) {
                            Log::setLog("Принудительное удаление тоже не помогло!!! $file Может забиться сервер!!!!");
                        }

                    }
                    if (!file_exists($file)) return true;
                } else {
                    return true;
                }


            } else {
                Log::setError("Не удалось создать директорию $fullPath");
            }
        }
        return false;
    }
}