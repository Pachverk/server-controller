<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.06.2019
 * Time: 13:30
 */

namespace Pachverk;


use mysql_xdevapi\Exception;

class Mysql
{
    /** @var Mysql */
    private static $instance;
    private $connect;
    private $type;

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

    /**
     * Коннект будет создан ровно 1 раз
     * Механизма перебить или создать новый пока нет (нет надобности)
     * @param string $login
     * @param string $password
     * @param string $mysqlType
     * @return bool
     */
    public static function init($login = '', $password = '', $mysqlType = '')
    {
        if (isset(self::$instance->connect)) return true;
        if (empty($login)) $login = Settings::getOption('mysqlLogin');
        if (empty($password)) $password = Settings::getOption('mysqlPassWord');
        if (empty($mysqlType)) $mysqlType = Settings::getOption('mysqlType');
        if (empty($mysqlType)) $mysqlType = 'mysqli';

        self::$instance       = new self;
        self::$instance->type = $mysqlType;

        if (!empty($login))
        switch (self::$instance->type) {
            case 'pdo':
                if (class_exists(\PDO::class)) {
                    self::$instance->connect = new \PDO("mysql:host=localhost;", $login, $password);
                }
                break;
            case 'mysql':
                if (function_exists('mysql_connect')) {
                    self::$instance->connect = mysql_connect('localhost', $login, $password);
                }
                break;
            default:
                if (function_exists('mysqli_connect')) {
                    self::$instance->connect = mysqli_connect('localhost', $login, $password);
                }
                break;
        }
        return (self::$instance->connect) ? true : null;
    }

    static function getDatabases()
    {
        return self::query('show databases');
    }

    static function query($str)
    {
        if (!self::$instance->connect) {
            if (class_exists(Log::class)) Log::setError('Не установлено подключенеи к базе данных');
            return;
        }

        switch (self::$instance->type) {
            case 'pdo':
                /*
                 * PDO::FETCH_ASSOC — возвращает массив, индексированный по имени столбца в таблице базы данных;
                 * PDO::FETCH_NUM — возвращает массив, индексированный по номеру столбца;
                 * PDO::FETCH_OBJ — возвращает анонимный объект с именами свойств, соответствующими  именам столбцов.
                 * Например, $row->id будет содержать значение из столбца id.
                 * PDO::FETCH_CLASS — возвращает новый экземпляр класса, со значениями свойств, соответствующими данным
                 * из строки таблицы. В случае, если указан параметр PDO::FETCH_CLASSTYPE (например PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE),
                 * имя класса будет определено из значения первого столбца.
                 */
                $obRes = self::$instance->connect->query($str);
                if (is_object($obRes)) {
                    $res = [];
                    while($row = $obRes->fetch(\PDO::FETCH_NUM)) {
                        $res[] = current($row);
                    }
                }
                break;
            case 'mysql':
                $res_sql = mysql_query($str);
                while ($row = mysql_fetch_array($res_sql)) {
                    $res[] = current($row);
                }
                if ($error = mysql_error(self::$instance->connect)) {
                    Log::setError($error);
                }
                break;
            case 'mysqli':
            default:
                $res_sql = mysqli_query(self::$instance->connect, $str/*, MYSQLI_USE_RESULT*/);
                while ($row = mysqli_fetch_array($res_sql)) {
                    $res[] = current($row);
                }
                if ($error = mysqli_error(self::$instance->connect)) {
                    Log::setError($error);
                }
                break;
        }
        return $res;
    }

    static function fixAllRatings()
    {
        foreach (self::getDatabases() as $dbName) {
            self::query("use `$dbName`;");
            $tables = self::query('show tables');
            if (in_array('b_rating', $tables)) {
                self::query('TRUNCATE TABLE b_rating;');
                self::query('DROP TABLE b_rating_prepare;');
                self::query('CREATE TABLE b_rating_prepare(ID int(11) NULL);');
                if (class_exists(Log::class)) Log::setSuccess('Была обработана база данных ' . $dbName);
            }
        }
    }

    /**
     * Список пользователей работающих с базами данных
     * @return array|void
     */
    static function getUsers()
    {
        self::query("use mysql");
        return self::query("select distinct user from user");
    }

    /**
     * Генерация имени базы данных на основе id сайта
     * @param string $siteID
     * @return mixed|string
     */
    static function getDBNameBySiteID($siteID = '')
    {
        $siteID = strtolower($siteID);

        $tmp = $name = str_replace(['.com', '.ua', '.ru',
            'test',
            'https', 'http', ':', '/', '.', '-'
        ], '', $siteID);

        $dbList = self::getDatabases();

        $iterator = 0;
        while (in_array($name, $dbList)) {
            $iterator ++;
            $name = $tmp.$iterator;
        }

        return $name;
    }

    /**
     * При успешном создание базы данных и пользователя вернет истину
     * @todo Нужно переименовать метод поскольку он создаст не только базу но и пользователя с привилегиями
     * Create new database for user
     * @param string      $dbName
     * @param string      $dbUser
     * @param string      $dbPassword
     * @return bool
     */
    static function createDatabase($dbName, $dbUser, $dbPassword)
    {
        if (!self::init()) return false;

        if (empty($dbName)) {
            Log::setError('Не указао имя базы данных');
            return false;
        }
        if (empty($dbUser)) {
            Log::setError('Не указан пользователь базы данныъ');
            return false;
        }
        if (empty($dbPassword)) {
            Log::setError('Не указан пароль для пользователя системы');
            return false;
        }
        if (in_array($dbUser, self::getUsers())) {
            Log::setError('Такой пользователь уже есть в системе, укажите нового или нужно исправить логику добавления быза данных');
            return false;
        }

        // Создание самой базы данных
        if (!in_array($dbName, self::getDatabases())) {
            $res = self::query("CREATE DATABASE `{$dbName}` DEFAULT character SET utf8 COLLATE utf8_unicode_ci;");
            if (in_array($dbName, self::getDatabases())) {
                Log::setSuccess("Успешно создана база данных $dbName");
            } else {
                Log::setError("По непонятным причинам базаданных не смогла создаться ($dbName)");
                return false;
            }
        } else {
            Log::setError("База данных($dbName) уже существует укажите новую ");
            return false;
        }

        // Создаю пользователей в случае успешной созданной базы
        if (1) {
            self::query("GRANT ALL PRIVILEGES ON `{$dbName}`.* TO '{$dbUser}'@'%' IDENTIFIED BY '{$dbPassword}';");
            self::query("GRANT ALL PRIVILEGES ON `{$dbName}`.* TO '{$dbUser}'@'localhost' IDENTIFIED BY '{$dbPassword}';");
            self::query("FLUSH PRIVILEGES;");
            if (in_array($dbUser, self::getUsers())) {
                Log::setSuccess("Успешно создан пользователь $dbUser для базы данных $dbName");
            } else {
                Log::setError("Не удалось создать пользователя базы даннх нужно проверить что пошло не так ($dbUser)");
                return false;
            }
        }

        return true;
    }

    /**
     * Удаление всех созданных пользовательских полей базы данных
     * @var string $user
     * @return bool
     */
    static function deleteUser($user)
    {
        if (empty($user)) {
            Log::setError('Нужно указать имя пользователя для его удаления');
            return false;
        }
        if (in_array($user, self::getUsers())) {
            self::query("DROP USER '$user'@'localhost';");
            self::query("DROP USER '$user'@'%';");
            self::query("FLUSH PRIVILEGES;");

            if (!in_array($user, self::getUsers())) {
                return true;
            } else {
                Log::setError("Не удалось удалить пользователя($user) нужно проверить в чем ошибка");
            }

        } else {
            Log::setError("Пользователь не нашелся в системе({$user}) для удаления");
        }
        return false;
    }

    /**
     * Удаляет базу данных если нашло такую
     * @var string $dbName
     * @return bool
     */
    static function deleteDatabase($dbName)
    {
        $databases = self::getDatabases();
        if (in_array($dbName, $databases)) {
            self::query("drop database `{$dbName}`");
            if (in_array($dbName, self::getDatabases())) {
                Log::setError("Что то пошло не так в момент удаления базы данныъ ($dbName), нужно перепроверить метод");
            }
            return true;
        } else {
            Log::setError("Такой базы не существует ({$dbName})");
            return false;
        }
    }

    static function install() {
        // SET NAMES utf8;
        // SET time_zone = '+00:00';
        // SET foreign_key_checks = 0;
        // SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';
        //
        // DROP TABLE IF EXISTS `sites`;
        // CREATE TABLE `sites` (
        //         `id` int(11) NOT NULL AUTO_INCREMENT,
        //   `site_id` text COLLATE utf8_unicode_ci NOT NULL,
        //   `db_name` text COLLATE utf8_unicode_ci,
        //   `db_login` text COLLATE utf8_unicode_ci,
        //   `db_password` text COLLATE utf8_unicode_ci,
        //   PRIMARY KEY (`id`),
        //   UNIQUE KEY `site_id` (`site_id`(100))
        // ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        //
        // INSERT INTO `sites` (`id`, `site_id`, `db_name`, `db_login`, `db_password`) VALUES
        //         (4,	'some.test',	'test',	'test',	'test');
    }

    static function addConfig(Site $site) {
        self::query('use yam_config;');
        $query = "INSERT INTO `sites` (`site_id`, `db_name`, `db_login`, `db_password`) VALUES('{$site->id}',	'{$site->dbName}',	'{$site->dbUser}',	'{$site->dbPassword}');";
        self::query($query);
    }

    static function dump($dbName, $putPath) {
        exec("mysqldump -u root $dbName > $putPath");
    }
}