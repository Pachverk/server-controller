<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.08.2019
 * Time: 9:19
 */

namespace Pachverk;


class Domain
{
    static function getDomainInfo($domain) {
        $rest = new Rest();
        $rest->path = "https://htmlweb.ru/analiz/api.php?whois&url=$domain&json";
        $rest->dataType('json');
        $info = $rest->get();
        return $info;
    }
    static function getEndDate($domain)
    {
        $info = self::getDomainInfo($domain);
        return $info->paid;
    }
}