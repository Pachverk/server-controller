<?php


namespace Pachverk;


class Users
{
    static  $usersFile = '/etc/passwd';
    private $user;
    private $group;
    private $homeDir;

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function setGroup($group)
    {
        $this->group = $group;
    }

    public function setHomeDir($homeDir)
    {
        $this->homeDir = $homeDir;
    }

    static function getUsers()
    {

        if (!file_exists(self::$usersFile)) {
            return false;
        }

        $file = file(self::$usersFile);
        foreach ($file as $string) {
            $tmpUser   = explode(':', $string);
            $arUsers[] = current($tmpUser);
        }

        return $arUsers;
    }

    /**
     * Создаст пользователя в системе линукс с его корневой директорией
     * @param string $userName Имя линкс пользователя
     * @param string $passWord Пароль в дальнейшем можно будет указывать
     * @return bool статус создания
     */
    static function createUser($userName, $passWord = 'random')
    {
        $users = self::getUsers();
        if (in_array($userName, $users)) {
            return true;
        }

        $log = shell_exec("adduser {$userName} --home /home/{$userName}");
        if (!empty($log)) {
            if (class_exists(Log::class)) Log::setError(trim($log));
            return false;
        }

        return true;
    }

    static function getDefaultUserName(Site $site) {
        $linuxUserName = $site->id;
        $linuxUserName = str_replace([
            '.ru', '.com', '.ua', 'gz', '.org', '.net',
            '.kiev', '.com.ua', '.sk', '.az', '.at', '.in', '.club'
        ], '', $linuxUserName);
        $linuxUserName = str_replace(['.', '-'], '', $linuxUserName);
        if (strlen($linuxUserName) < 4) {
            $linuxUserName .= 'user';
        }
        return $linuxUserName;
    }
}