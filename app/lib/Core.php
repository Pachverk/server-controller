<?php


namespace Pachverk;
use Composer\Script\Event;
use mysql_xdevapi\Collection;

class Core
{
    static $result;
    static function setResult($arg) {
        self::$result = $arg;
    }

    static function getResult() {
        return self::$result;
    }

    static function compactData($data) {
        $data = serialize($data);
        $data = base64_encode($data);
        return $data;
    }

    /**
     * Распаковывает информацию с запроса
     * @param $data
     * @return bool|mixed|string
     */
    static function unpackData($data) {
        $response = $data;
        $response = base64_decode($response);
        $response = unserialize($response);
        return $response;
    }

    static function getParams($argv) {
        $params = [];
        if (is_array($argv)) {
            foreach ($argv as $param) {
                if (strpos($param, '--') !== false) {
                    $param = str_replace('--', '', $param);
                    $param = explode('=', $param);
                    $params[$param[0]] = $param[1];
                } else {
                    $params[] = $param;
                }
            }
        }
        return $params;
    }

    static function monit($argv)
    {
        $params = self::getParams($argv);

        /**
         * @var Site $site
         */
        switch ($argv[1]) {
            case 'bitrixClearDir': // TODO Тело содержимого нужно куда-то перенести
                $saveDbConn = true;
                $clearDir   = true;
                $clearDB    = true;
                $saveModule = true;
                $backModule = true;
                $cpBaseDir  = true;

                $site = Site::getSiteByID(basename($_SERVER['PWD']));
                if (empty($site)) break;

                $dbconn = $site->documentRoot.'/bitrix/php_interface/dbconn.php';
                $settings = $site->documentRoot.'/bitrix/.settings.php';
                $moduleName = Settings::getOption('mainModule');
                $modulePath = $site->documentRoot.'/bitrix/modules/'.$moduleName;

                if (!empty(Settings::getOption('mainModule'))) {
                    // Сохранить соединения в переменные
                    if ($saveDbConn) {
                        if (file_exists($dbconn)) {
                            $data[$dbconn] = file_get_contents($dbconn);
                        }
                        if (file_exists($settings)) {
                            $data[$settings] = file_get_contents($settings);
                        }
                    }

                    // Очистить полностью базу данных
                    if ($clearDB) {
                        if(file_exists($dbconn)) {
                            if (file_exists($settings)) {
                                $info = require $settings;
                                $db = $info['connections']['value']['default'];
                                $dumpFile = '/tmp/backups/'.basename($_SERVER['PWD']).'/'.date('Y.m.d.h.i.s.').$db['database'].'.sql';
                                exec("mysqldump -u root {$db['database']} > $dumpFile");
                                if (file_exists($dumpFile)) {
                                    Log::setSuccess("Успешно сохранена база $dumpFile");
                                    if (!empty($db['database']) && Mysql::init()) {
                                        $query = "use {$db['database']}";
                                        Mysql::query($query);
                                        $tables = Mysql::query('show tables;');
                                        if (!empty($tables)) {
                                            foreach ($tables as $table) {
                                                $query = "DROP TABLE $table";
                                                Log::setSuccess($query);
                                                Mysql::query($query);
                                            }
                                        }
                                    }
                                } else {
                                    Log::setError("Fail save the dump file $dumpFile");
                                }
                            } else {
                                $info = require $dbconn;
                            }

                        } else {
                            Log::setError("Не нашелся файл $dbconn");
                        }
                    }

                    // Спрятать модуль в безопстную директорию на момент манипуляций
                    if ($saveModule) {
                        if (is_dir($modulePath)) {
                            exec("mv $modulePath /tmp/$moduleName");
                        }
                    }

                    // Удаление директории
                    if ($clearDir) {
                        $comand = "rm -rf {$site->documentRoot}/*";
                        exec("rm -rf $comand");
                    }

                    // Копирование файлов по умолчанию
                    if ($cpBaseDir) {
                        $mainDir = realpath($_SERVER['PHP_SELF']);
                        $mainDir = dirname($mainDir);
                        $defaultFilesForSpace = $mainDir.'/app/files/siteDir';

                        if (is_dir($defaultFilesForSpace)) {
                            exec("cp -r $defaultFilesForSpace/* {$site->documentRoot}");
                            if ($backModule) mkdir(dirname($modulePath), 0755);
                            exec("chown -R {$site->linuxUser}:{$site->linuxUser} {$site->documentRoot}");
                        }
                    }

                    // Копирнуть модуль на место
                    if ($backModule) {
                        if (is_dir('/tmp/'.$moduleName)) {
                            exec("mv /tmp/$moduleName $modulePath");
                            exec("chown -R root:root $modulePath");
                        }
                    }

                    // Залить сохраненные файлы соединения на место
                    if ($saveDbConn) {
                        if (!empty($data)) {
                            foreach ($data as $file => $content) {
                                file_put_contents($file, $content);
                            }
                        }
                    }

                } else {
                    Log::setError("Не указан главный модуль для сохранения работы 'mainModule'");
                }
                break;
            case 'delete':
                if (!empty($params['id'])) {
                    foreach (Site::getSites() as $site) {
                        if ($site->id === $params['id']) {
                            if ($site->delete()) self::setResult(true);
                            break;
                        }
                    }
                }
                break;
            case 'fixBug':
                switch ($params[2]) {
                    case 'httpd': HotFixer::fixHttpd(); break;
                    case 'nginx':
                        $sites = Httpd::getSiteList();
                        foreach ($sites as $site) {
                            $site->generateNginxFile();
                        }
                        break;
                    case 'links': HotFixer::changeLinksToDir(); break;
                    case 'closeModules': HotFixer::closeModules(); break;
                    case 'changeDirsFromSiteToSites': HotFixer::changeDirsFromSiteToSites(); break;
                    case 'clearFTP': HotFixer::clearFTPServer(); break;
                    default: HotFixer::fixAllProblems(); break;
                    // case 'changeSettingsToNewStandard': HotFixer::changeSettingsToNewStandard(); break;
                }
                break;
            case 'updateSite':
                if (!empty($params['site'])) {
                    $site = self::unpackData($params['site']);
                }

                if (get_class($site) === Site::class) {
                    if ($site->update()) {
                        Log::setSuccess("Обновден конфиг файл сайта [{$site->id}]");
                    } else {
                        Log::setError("Провалено обновление конфиг файл сайта [{$site->id}]");
                    }
                }

                break;
            case 'test':
                UnitTest::run();
                break;
            case 'testPdo':
                Mysql::init();
                $res = Mysql::query('show databases');
                print_r($res);
                break;
            case 'apiCreateWebSpace':
                if (!empty($params['site'])) {
                    $params['site'] = base64_decode($params['site']);
                    $params['site'] = unserialize($params['site']);
                    $site = $params['site'];
                    if (get_class($site) === Site::class) {

                        if (!Mysql::init()) {
                            Log::setError('Нет соединения с базой данных');
                        }

                        if (!Site::checkEmptySiteID($site->id)) {
                            Log::setError("Сайт с таким ID({$site->id}) уже существует на сервере, укажите какой-то другой");
                        }

                        $arExists = Site::getDomainsAlreadyExists($site->domains);
                        if (!empty($arExists)) {
                            $existsDomains = implode(', ', $arExists);
                            Log::setError("Домены которые уже заняты в системе: $existsDomains");
                        }

                        if (!Log::getErrors()) {
                            Log::setLog('Запущен статус создания окружения');

                            // Формирование данных для базы данных
                            if (1) {
                                $site->dbUser = $site->dbName = Mysql::getDBNameBySiteID($site->id);
                                $site->dbPassword = Tools::generatePassword(8);
                            }

                            Bitrix::createWebSpace($site);
                        }

                    } else {
                        Log::setError('Для создания окружения должны совпадать обекты сайтов. Прошел конфликт обратитесь к администратору');
                    }

                } else {
                    Log::setError('Не указаны входные параметры для создания сайта');
                }

                $result = [
                    'site' => $site,
                    'log' => Log::getLog(),
                    'errors' => Log::getErrors(),
                    'success' => Log::getSuccess(),
                ];
                $result = serialize($result);
                $result = base64_encode($result);
                echo $result;
                die();
                break;
            case 'disbleSite':
                $site = Site::getSiteByID($params['id']);
                if (is_object($site)) {
                    $site->disableSite();
                } else {
                    Log::setError("Не нашлось нужного сайта");
                }
                $result = [
                    'params' => $params,
                    'log' => Log::getLog(),
                    'errors' => Log::getErrors(),
                    'success' => Log::getSuccess(),
                ];
                echo self::compactData($result);
                die();
                break;
            case 'activeteSite':
                $site = Site::getSiteByID($params['id']);
                if (is_object($site)) {
                    $site->activeteSite();
                } else {
                    Log::setError("Не нашлось нужного сайта");
                }
                $result = [
                    'params' => $params,
                    'log' => Log::getLog(),
                    'errors' => Log::getErrors(),
                    'success' => Log::getSuccess(),
                ];
                echo self::compactData($result);
                die();
                break;
            case 'getSite':
                $site = Site::getSiteByID($params['id']);

                if (!is_object($site)) {
                    Log::setError("Не нашлось нужного сайта");
                }

                Core::setResult($site);

                /*
                $result = [
                    'site' => $site,
                    'incParmas' => $params,
                    'log' => Loger::getLog(),
                    'errors' => Loger::getErrors(),
                    'success' => Loger::getSuccess(),
                ];
                echo self::compactData($result);
                die();
                */
                break;
            case 'getSize':
                if (!empty($params['dir'])
                    && is_dir($params['dir'])
                    && !in_array($params['dir'], ['.', '..', '/', ''])
                ) {
                    echo Tools::getDirSize($params['dir']);
                }
                break;
            case 'getSites':
                echo base64_encode(serialize(Site::getSites()));
                break;
            case 'getSitesWithProblem':
                $sites = Site::getSitesWithProblem();
                Core::setResult($site);
                break;
            case 'getCertWithoutSite':
                self::setResult(Core::getCertWithoutSite());
                break;


            case 'newDB':
                $db = new DB;
                $db->name = $params['name'];
                $db->password = $params['password'];
                Core::setResult([
                    'status' => $db->create()
                ]);
                break;
            case 'showSiteByID':
                d(Site::getSiteByID($params[2]));
                break;
            case 'findSiteByDomain':
                // Уникальный идентификатор сайта
                do {
                    echo 'Укажите домен сайта который интересует:'.PHP_EOL;
                    $domain = trim(fgets(STDIN));
                } while (empty($domain));
                d(Site::getSiteByDomain($domain));
                break;
            case 'addNewPubKey':
                $arSites = Httpd::getSiteList();
                do {
                    echo 'Укажите публичный ключ пользователя: '.PHP_EOL;
                    $key = trim(fgets(STDIN));
                } while (empty($key));
                foreach ($arSites as $site) {
                    if (!is_dir("/home/{$site->linuxUser}/.ssh")) {
                        mkdir("/home/{$site->linuxUser}/.ssh", 0700);
                    }
                    file_put_contents("/home/{$site->linuxUser}/.ssh/authorized_keys", $key.PHP_EOL, FILE_APPEND);
                    exec("chown -R {$site->linuxUser}:{$site->linuxUser} /home/{$site->linuxUser}/.ssh");
                }
                break;
            case 'closeDir':
                exec('chown -R root:root ./');
                exec('find ./ -type d -exec chmod 0711 {} \;');
                exec('find ./ -type f -exec chmod 0604 {} \;');
                break;
            case 'hgToRoot':
                exec("find ./ -d -name '.hg'  -exec chown -R root:root {} \\;");
                break;
            case 'UnitTest':
                UnitTest::run();
                break;
            case 'createTarDir':
                if (!empty($params[2])) {
                    $path = $params[2];
                } else {
                    echo 'Укажите путь:'.PHP_EOL;
                    $path = trim(fgets(STDIN));
                }

                if (file_exists(realpath($path))) {
                    $base = basename($path);
                    $date = date('d.m.Y');
                    $comand = "tar -cvf $date.{$base}.tar $path";
                    exec($comand);

                    if (file_exists("$date.{$base}.tar")) {
                        Log::setSuccess("Архив создан успешно $date.{$base}.tar");
                    } else {
                        Log::setError("Не удалось создать архив $date.{$base}.tar");
                    }
                }

                break;
            case 'createBackups':
                echo 'Создание бекапов запущено ...' . PHP_EOL;
                Backup::createBackups();
                break;
            case 'backupMysql':
                echo 'Создание дампов баз данных' . PHP_EOL;
                Backup::backupMysql();
                break;
            // TODO на время разработки пока будет в будущем будет снесено
            case 'fixTablesRatings':
                if (Mysql::init()) {
                    Mysql::fixAllRatings();
                }
                break;
            case 'deleteAllBackupsAndCache':
                Bitrix::deleteCaches();
                Bitrix::deleteBackups();
                break;
            case 'searchVirus':
                Bitrix::findVirusInSites();
                break;
            // TODO решение нужно будет снести (Нужно только для дебага)
            case 'searchSitesWithPropblems':
                Bitrix::monitoringSites();
                break;
            // TODO В дальнейшом будет убит класс Bitrix и перенесенно в класс Site
            case 'createWebSpace':
                $site = new Site();
                if (!Mysql::init()) {
                    if (class_exists(Log::class)) Log::setError('Нет соединения с базой данных');
                    break;
                }

                // Уникальный идентификатор сайта
                do {
                    echo 'Укажите пожайлуйста ID сайта (название папки):'.PHP_EOL;
                    $site->id = trim(fgets(STDIN));
                } while (Site::checkEmptySiteID($site->id) == false);

                // Уникальное имя в линукс системе
                // Проверка на уникальность
                if (0) {
                    do {
                        echo 'Укажите имя пользователя в линукс системе'.PHP_EOL;
                        $site->linuxUser = trim(fgets(STDIN));
                    } while (Linux::checkEmptyUser($site->linuxUser) == false);
                } else {
                    echo 'Укажите имя пользователя в линукс системе'.PHP_EOL;
                    $site->linuxUser = trim(fgets(STDIN));
                }

                // Прописка всех доменов для сайта
                do {
                    echo 'Укажите домены сайта (указывать без лишнего через пробел)'.PHP_EOL;
                    $stringDomains = trim(fgets(STDIN));
                    $stringDomains = str_replace(['http', 'https', '/', ':'], '', $stringDomains);
                    $site->domains = explode(' ', $stringDomains);
                    $arExists = Site::getDomainsAlreadyExists($site->domains);
                    if (!empty($arExists)) {
                        echo 'Домены которые уже заняты в системе: '.PHP_EOL;
                        echo implode(', ', $arExists).PHP_EOL;
                    }
                    $status = (!empty($arExists)) ? true : false;
                } while ($status);

                // Формирование данных для базы данных
                if (1) {
                    $site->dbUser = $site->dbName = Mysql::getDBNameBySiteID($site->id);
                    $site->dbPassword = Tools::generatePassword(8);
                }

                Bitrix::createWebSpace($site);
                break;
            case 'showList':
                $list = Httpd::getSiteList();
                foreach ($list as $site) {
                    echo "{$site->id}: ".PHP_EOL;
                    echo "\tdocumentRoot: {$site->documentRoot}".PHP_EOL;
                    $domains = implode(', ', $site->domains);
                    echo "\tdomains: {$domains}".PHP_EOL;
                }
                break;
            case 'restart':
                Httpd::restart();
                Nginx::restart();
                break;
            default:
                self::info();
                break;
        }

        if (isset($params['api']) && $params['api'] == 'Y') {
            $result = [
                'result' => self::getResult(),
                'incParams' => $params,
                'log' => Log::getLog(),
                'errors' => Log::getErrors(),
                'success' => Log::getSuccess(),
            ];
            echo self::compactData($result);
        } else {
            $result = self::getResult();
            if (isset($result)) d($result);
            Log::printAll();
        }
    }

    static function info()
    {
        $arListOfCommands = [
            'findSiteByDomain'         => 'Найти инфу о сайте по его домены',
            'UnitTest'                 => 'Запуск обявленных тестов тестов',
            'createBackups'            => 'Создать бэкапы сайтов',
            'backupMysql'              => 'Создать дампов баз данных',
            'fixTablesRatings'         => 'Фикснуть базы данных от рейтингов',
            'deleteAllBackupsAndCache' => 'Удалить все бекапы и кеши',
            'searchVirus'              => 'Проверка сайтов на вирусы',
            'searchSitesWithPropblems' => 'Проверка сайтов на наличие проблем',
            'createWebSpace'           => 'Создать веб окружение сайта (Пока на етапе теста)',
            'showList'                 => 'Показать краткую инфу о сайтах',
            'hgToRoot'                 => 'Смена пользователя на рут для контроля версий',
            'closeDir'                 => 'Прикрыть директорию от любопытных',
            'addNewPubKey'             => 'Добавить еще один публичный ключ для всех сайтов',
            'createTarDir'             => 'Создать архив диретории, можно передать сразу след аргументом путь',
            'restart'                  => 'Перезагрузка сервера Httpd Nginx',
            'getSitesWithProblem'      => 'Получить перечень проблемных сайтов',
            'getCertWithoutSite'       => 'Показать сертификаты без сайтов',
            'newDB'                    => 'Создать новую базу данных (--name= --password)',
        ];
        echo 'Перечень возможных команд: ' . PHP_EOL;
        foreach ($arListOfCommands as $code => $action) {
            echo "\t$code ({$action})" . PHP_EOL;
        }
    }

    static function init()
    {
        $obj       = new self();
        $arActions = [
            'createBackups' => [
                'name' => 'Создать бекапы сайтов которые есть в наличие',
            ],
            'createBackup'  => [
                'name'     => 'Создать бекап сайта',
                'callBack' => [$obj, 'createBackup'],
            ],
        ];
        echo 'Выберите что вы хотите запустить:' . PHP_EOL;
        foreach ($arActions as $code => $action) {
            echo "\t$code ({$action['name']})" . PHP_EOL;
        }

        $codeAction = trim(fgets(STDIN));

        if (array_key_exists($codeAction, $arActions)) {
            call_user_func($arActions[$codeAction]['callBack']);
        }
    }

    public function selectSite()
    {
        $siteList = Httpd::getSiteList();
        echo 'Какой сайт нужно забэкапить:' . PHP_EOL;
        foreach ($siteList as $siteName) {
            echo "\t" . $siteName . PHP_EOL;
        }
        $siteSelected = trim(fgets(STDIN));
        if (in_array($siteSelected, $siteList)) {
            return $siteSelected;
        } else {
            echo 'Такого сайта не существует выберите еще раз:' . PHP_EOL;
            return $this->selectSite();
        }
    }

    public function createBackup()
    {
        $site = $this->selectSite();
        echo "Site; $site" . PHP_EOL;
    }

    /**
     * Установка выполнимого файла в нужном пространстве имен
     * ВАЖНО! Команда будет доступна всем у кого есть доступ к чтению и выполнению к данной директории с решением
     *
     * PHP:
     * symlink(__DIR__.'/gcore', '/usr/local/bin/gcore');
     *
     * shel:
     * ln -s $PWD/gcore /usr/local/bin/gcore
     */
    static function install(Event $event) {
        $fileName = 'gcore';

        // Переменная HOME есть в убунте на других серваках нужно будет перепроверить
        if (!empty($_SERVER['HOME']) && is_dir($_SERVER['HOME'].'/.local/bin')) {
            $pathTo = $_SERVER['HOME'].'/.local/bin/'.$fileName;
        } else {
            $pathTo = '/usr/local/bin/'.$fileName;
        }

        $dir = dirname($event->getComposer()->getConfig()->get('vendor-dir'));
        if (is_dir('/usr/local/')
            && !file_exists($pathTo)
            && file_exists($dir.'/gcore')
        ) {
            chmod($dir.'/gcore', 0755);
            symlink($dir.'/gcore', $pathTo);
        }

        if (!file_exists($dir.'/settings.php')) {
            copy($dir.'/simple.settings.php', $dir.'/settings.php');
        }
    }


    static function getCertWithoutSite() {
        /**
         * @var Site $site
         */

        $certs = scandir('/etc/nginx/ssl');
        $certs = array_diff($certs, ['.', '..']);
        $certsExist = [];
        $sites = Site::getSites();
        foreach ($sites as $site) {
            if (file_exists("/etc/nginx/ssl/{$site->id}.pem")) {
                $certsExist[] = "{$site->id}.pem";
            }
        }

        $certs = array_diff($certs, $certsExist);
        return $certs;
    }
}