<?php


namespace Pachverk;


class DB
{
    public $name = '';
    public $user = '';
    public $password = '';
    public $connectionType = '';
    public function validate():bool
    {
        return true;
    }
    public function create():bool
    {
        $status = true;

        if (!class_exists(Mysql::class)) {
            Log::setError('class not exist '.Mysql::class);
            $status = false;
        }

        if (empty($this->name)) {
            Log::setError('Db name not isset ($this->name)');
            $status = false;
        }

        if (empty($this->password)) {
            Log::setError('Db password not isset ($this->password)');
            $status = false;
        }

        if (!Mysql::init()) {
            Log::setError('Mysql not connection');
            $status = false;
        }

        if ($status === true) {

            if (empty($this->user)) {
                $this->user = $this->name;
            }

            $status = Mysql::createDatabase($this->name, $this->user, $this->password);
        }
        
        return $status;
    }
}