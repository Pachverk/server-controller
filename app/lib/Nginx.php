<?php


namespace Pachverk;


class Nginx
{
    static $pathSiteConfig = '/etc/nginx/sites';
    static $pathSiteConfigAvaliable = '/etc/nginx/bx/site_avaliable';
    static $pathSiteConfigEneble = '/etc/nginx/bx/site_enabled';
    static $pathSaveConfigs = '/etc/nginx/olg.conf';
    static $pathSsls = '/etc/nginx/ssl';

    /**
     * @param Site   $site
     * @param string $newRootDir
     * @param string $oldDir Не обязательный можно явно указать страую директорию которую нужно заменить (нужно для хот фиксов)
     * @return bool
     */
    static function changeRootDir(Site $site, $newRootDir, $oldDir='')
    {
        if (empty($oldDir)) {
            $oldDir = $site->documentRoot;
        }

        if (!is_dir(self::$pathSaveConfigs)) {
            if (!mkdir(self::$pathSaveConfigs, 700, true)) {
                if (class_exists(Log::class)) Log::setError($site->id.' Не удалось создать директории для хранения старых настроек nginx');
                return false;
            }
        }

        if (!file_exists($site->nginxConfigFile)) {
            if (class_exists(Log::class)) Log::setError($site->id.' Нету файла с настройками ' . $site->nginxConfigFile);
            return false;
        }

        if (!file_exists($site->nginxSSLConfigFile)) {
            if (class_exists(Log::class)) Log::setError($site->id.' Нету файла с настройками ' . $site->nginxSSLConfigFile);
            return false;
        }

        $fileBack = self::$pathSaveConfigs . '/' . date('Y.m.d.') . basename($site->nginxConfigFile);
        if (!copy($site->nginxConfigFile, $fileBack)) {
            if (class_exists(Log::class)) Log::setError($site->id.' Не удалось сохранить резервную копию настройки nginx ' . $site->nginxConfigFile);
            return false;
        }

        $fileBack = self::$pathSaveConfigs . '/' . date('Y.m.d.') . basename($site->nginxSSLConfigFile);
        if (!copy($site->nginxSSLConfigFile, $fileBack)) {
            if (class_exists(Log::class)) Log::setError($site->id.' Не удалось сохранить резервную копию настройки nginx ' . $site->nginxSSLConfigFile);
            return false;
        }

        $fileOriginal = $fileData = file_get_contents($site->nginxConfigFile);
        $fileData     = str_replace($oldDir, $newRootDir, $fileData);
        if (!file_put_contents($site->nginxConfigFile, $fileData)) {
            if (class_exists(Log::class)) Log::setError($site->id.' Не удалось сохранить настройки nginx ' . $site->nginxConfigFile);
            return false;
        }

        $fileData = file_get_contents($site->nginxSSLConfigFile);
        $fileData = str_replace($oldDir, $newRootDir, $fileData);
        if (!file_put_contents($site->nginxSSLConfigFile, $fileData)) {
            if (class_exists(Log::class)) Log::setError($site->id.' Не удалось сохранить настройки nginx ' . $site->nginxSSLConfigFile);
            file_put_contents($site->nginxConfigFile, $fileOriginal);
            return false;
        }

        return true;
    }

    static function getPathSslFile($siteID) {
        return self::$pathSsls."/{$siteID}.pem";
    }

    static function getPathConfigFile($siteID)
    {
        return self::$pathSiteConfig."/{$siteID}.conf";
    }


    static function rollBack(Site $site) {
        $fileBack = self::$pathSaveConfigs . '/' . date('Y.m.d.') . basename($site->nginxConfigFile);
        copy($fileBack, $site->nginxConfigFile);
        $fileBack = self::$pathSaveConfigs . '/' . date('Y.m.d.') . basename($site->nginxSSLConfigFile);
        copy($fileBack, $site->nginxSSLConfigFile);
    }

    /**
     * Проверяет общее состояние сайта (краткий вариант)
     * @param Site $site
     * @return bool
     */
    static function status(Site $site) {
        if (!file_exists($site->nginxConfigFile)
        || !file_exists($site->nginxSSLConfigFile)
        ) {
            return false;
        } else {
            $data = file_get_contents($site->nginxConfigFile);

            $re = '/(\$docroot).*/';
            preg_match_all($re, $data, $matches, PREG_SET_ORDER, 0);
            $getInfo = current(current($matches));
            $getInfo = str_replace('$docroot', '', $getInfo);
            $getInfo = str_replace(['\'', '"', ';'], '', $getInfo);
            $getInfo = trim($getInfo);
            if ($getInfo !== $site->documentRoot) return false;

            return true;
        }
    }

    /**
     * Развернутое состояние состояния настроек
     * @TODO Метод yужно или удалить или привести в нормальный вид
     * @param Site $site
     * @return array
     */
    static function getArStatus(Site $site) {
        $nginxRoot = self::getRoot($site->nginxConfigFile);
        // $nginxSslRoot = self::getRoot($site->nginxSSLConfigFile);
        $site->nginx['root'] = $nginxRoot;
        // $site->nginx['rootSsl'] = $nginxSslRoot;
        return [
            'nginxConfigFile' => file_exists($site->nginxConfigFile),
            // 'nginxSSLConfigFile' => file_exists($site->nginxSSLConfigFile),
            'nginxDocumentRoot' => is_dir($nginxRoot) && $site->documentRoot === $nginxRoot,
            // 'nginxSslDocumentRoot' => is_dir($nginxSslRoot) && $site->documentRoot === $nginxSslRoot,
        ];
    }

    static function createNginxConfig(Site $site, $update=false) {
        $site->nginxConfigFile = self::getPathConfigFile($site->id);

        if (empty($site->sslFile)) {
            $site->sslFile = self::getPathSslFile($site->id);
        }

        // $site->sslFile = self::getPathSslFile($site->id);

        // if (empty($site->nginxConfigFile)) {
        //     Loger::setError("Не указан путь к конфиг файлу nginx");
        //     return false;
        // }

        // if (file_exists($site->nginxConfigFile)) {
        //     Loger::setError("Файл с конфигами nginx уже существует ({$site->nginxConfigFile})");
        //     return false;
        // }

        if (empty($site->domains)) {
            Log::setError('Для создания конфиг файла нужно указать домены');
            return false;
        }

        // if (!in_array($site->id, $site->domains))
        //     $site->domains[] = $site->id;
        // if (!in_array('www.'.$site->id, $site->domains))
        //     $site->domains[] = 'www.'.$site->id;

        $domains = array_unique($site->domains);
        $domains = implode(' ', $domains);
        $fileTemplate = '';
        $fileTemplate .= "server {\n";
        $fileTemplate .= "\tlisten 80 ;\n";
        $fileTemplate .= "\tserver_name {$domains};\n";
        $fileTemplate .= "\n";
        $fileTemplate .= "\taccess_log /var/log/nginx/{$site->id}_access.log main;\n";
        $fileTemplate .= "\terror_log  /var/log/nginx/{$site->id}_error.log warn;\n";
        $fileTemplate .= "\tserver_name_in_redirect off;\n";
        $fileTemplate .= "\n";
        $fileTemplate .= "\tproxy_set_header	X-Real-IP \$remote_addr;\n";
        $fileTemplate .= "\tproxy_set_header	X-Forwarded-For \$proxy_add_x_forwarded_for;\n";
        $fileTemplate .= "\tproxy_set_header	Host \$host:80;\n";
        $fileTemplate .= "\n";
        $fileTemplate .= "\tset \$proxyserver   \"http://127.0.0.1:8887\";\n";
        $fileTemplate .= "\tset \$imcontenttype	\"text/html; charset=utf-8\";\n";
        $fileTemplate .= "\tset \$docroot		\"{$site->documentRoot}\";\n";
        $fileTemplate .= "\n";
        $fileTemplate .= "\tindex index.php;\n";
        $fileTemplate .= "\troot \"{$site->documentRoot}\";\n";
        if (file_exists($site->sslFile)) {
            $fileTemplate .= "\n";
            $fileTemplate .= "\t# Redirect to ssl if need\n";
            $fileTemplate .= "\trewrite ^(.*)\$ https://\$host\$1 permanent;\n";
        }
        $fileTemplate .= "\t\n";
        $fileTemplate .= "\t# Include parameters common to all websites\n";
        $fileTemplate .= "\tinclude bx/conf/bitrix.conf;\n";
        $fileTemplate .= "\n";
        $fileTemplate .= "\t# Include munin and nagios web\n";
        $fileTemplate .= "\tinclude bx/server_monitor.conf;\n";
        $fileTemplate .= "}";

        if (file_exists($site->sslFile)) {
            $info = Server::getServerInfo();

            $fileTemplate .= "\nserver {\n";
            if ($info['release'] > 6) {
                $fileTemplate .= "\tlisten 443 http2;\n";
            } else {
                $fileTemplate .= "\tlisten 443;\n";
            }

            $fileTemplate .= "\n";
            $fileTemplate .= "\tserver_name {$domains};\n";
            $fileTemplate .= "\n";
            $fileTemplate .= "\tkeepalive_timeout 70;\n";
            $fileTemplate .= "\tkeepalive_requests  150;\n";
            $fileTemplate .= "\terror_page 497 https://\$host\$request_uri;\n";
            $fileTemplate .= "\tssl     on;\n";
            $fileTemplate .= "\tssl_protocols       TLSv1 TLSv1.1 TLSv1.2;\n";
            $fileTemplate .= "\tssl_ciphers         HIGH:!RC4:!aNULL:!MD5:!kEDH;\n";
            $fileTemplate .= "\tssl_certificate     {$site->sslFile};\n";
            $fileTemplate .= "\tssl_certificate_key {$site->sslFile};\n";
            $fileTemplate .= "\tssl_session_cache shared:SSL:10m;\n";
            $fileTemplate .= "\tssl_session_timeout 10m;\n";
            $fileTemplate .= "\tssl_prefer_server_ciphers on;\n";
            $fileTemplate .= "\n";
            $fileTemplate .= "\taccess_log /var/log/nginx/{$site->id}_access.log main;\n";
            $fileTemplate .= "\terror_log  /var/log/nginx/{$site->id}_error.log warn;\n";
            $fileTemplate .= "\t#charset utf-8;\n";
            $fileTemplate .= "\tserver_name_in_redirect off;\n";
            $fileTemplate .= "\n";
            $fileTemplate .= "\tproxy_set_header	X-Real-IP \$remote_addr;\n";
            $fileTemplate .= "\tproxy_set_header	X-Forwarded-For \$proxy_add_x_forwarded_for;\n";
            $fileTemplate .= "\tproxy_set_header	Host \$host:443;\n";
            $fileTemplate .= "\tproxy_set_header	HTTPS YES;\n";
            $fileTemplate .= "\tproxy_set_header	X-Forwarded-Proto https;\n";
            $fileTemplate .= "\n";
            $fileTemplate .= "\tset \$proxyserver	\"http://127.0.0.1:8887\";\n";
            $fileTemplate .= "\tset \$imcontenttype	\"text/html; charset=utf-8\";\n";
            $fileTemplate .= "\tset \$docroot		\"{$site->documentRoot}\";\n";
            $fileTemplate .= "\n";
            $fileTemplate .= "\tindex index.php;\n";
            $fileTemplate .= "\troot {$site->documentRoot};\n";
            $fileTemplate .= "\tproxy_ignore_client_abort off;\n";
            $fileTemplate .= "\n";
            $fileTemplate .= "\t# Include parameters common to all websites\n";
            $fileTemplate .= "\tinclude bx/conf/bitrix.conf;\n";
            $fileTemplate .= "\n";
            $fileTemplate .= "\t# Include munin and nagios web\n";
            $fileTemplate .= "\tinclude bx/server_monitor.conf;\n";
            $fileTemplate .= "}";
        }

        if (!file_exists($site->nginxConfigFile) || $update)

            if (!is_dir(self::$pathSiteConfig)) {
                mkdir(self::$pathSiteConfig, 0755, true);
            }

            if (!file_put_contents($site->nginxConfigFile, $fileTemplate)) {
                Log::setError("Не удалось создать конфиг файл nginx {$site->nginxConfigFile}");
                return false;
            }

        return true;
    }

    /*
    static function createNginxSslConfigFile(Site $site) {
        $fileTemplate = "server {
    listen 443 http2;

    server_name {$site->id} www.{$site->id};
    
    # keepalive_timeout 70;
    # keepalive_requests  150;
    # error_page 497 https://\$host\$request_uri;
    # ssl     on;
    # ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
    # ssl_ciphers         HIGH:!RC4:!aNULL:!MD5:!kEDH;
    # ssl_certificate     /etc/nginx/ssl/{$site->id}.pem;
    # ssl_certificate_key /etc/nginx/ssl/{$site->id}.pem;
    # ssl_session_cache shared:SSL:10m;
    # ssl_session_timeout 10m;
    # ssl_prefer_server_ciphers on;
    # access_log /var/log/nginx/{$site->id}_access.log main;
    # error_log  /var/log/nginx/{$site->id}_error.log warn;
    # #charset utf-8;
    # server_name_in_redirect off;

    proxy_set_header	X-Real-IP \$remote_addr;
    proxy_set_header	X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header	Host \$host:443;
    proxy_set_header	HTTPS YES;
    proxy_set_header	X-Forwarded-Proto https;

    set \$proxyserver	\"http://127.0.0.1:8887\";
    set \$imcontenttype	\"text/html; charset=#SERVER_ENCODING#\";
    set \$docroot		\"{$site->documentRoot}\";

    index index.php;
    root {$site->documentRoot};
    proxy_ignore_client_abort off;

    # Include parameters common to all websites
    include bx/conf/bitrix.conf;

    # Include munin and nagios web
    include bx/server_monitor.conf;
}";
        $file = self::getPathConfigSllFile($site->id);
        if (!file_exists($file))
            file_put_contents($file, $fileTemplate);
            symlink($file, '/etc/nginx/bx/site_enabled/'.basename($file));
    }*/

    static function getRoot($nginxFile) {
        if (!file_exists($nginxFile)) return;
        $data = file_get_contents($nginxFile);

        $reg = '/^(?!.*[#$]).*root[\t "]+(.+[a-zA-Z0-9])/m';
        preg_match_all($reg, $data, $matches, PREG_SET_ORDER, 0);
        $result = current($matches);
        $result = $result[1];
        return $result;
    }

    static function saveConfig($file)
    {
        $fileBack = self::$pathSaveConfigs . '/' . date('Y.m.d.') . basename($file);
        if (!file_exists($fileBack) && !copy($file, $fileBack)) {
            if (class_exists(Log::class)) Log::setError('Не удалось сохранить резервную копию настройки nginx ' . $file);
            return false;
        }
        return true;
    }

    static function restart() {
        $info = Server::getServerInfo();
        if ($info['release'] > 6) {
            exec('systemctl restart nginx');
        } else {
            exec('service nginx restart');
        }
    }

    /**
     * Снесет все возможные конфиги по конкретному сайту
     * @param Site $site
     * @return bool
     */
    static function deleteAllConfingsBySite(Site $site) {
        $site->checkNginxFiles();
        if ($site->nginxConfigFile) unlink($site->nginxConfigFile);
        return true;
    }

    static function hotFixer(Site $site, $version) {
        switch ($version) {
            case 'v1':
                /*
                $site->checkNginxFiles();
                if ($site->errors['nginxFileExist']) {
                    $oldFile = self::$pathSiteConfigAvaliable."/bx_ext_{$site->id}.conf";
                    $oldFileSsl = self::getPathConfigSllFile($site->id);

                    $configFile = '';
                    $site->nginxConfigFile = Nginx::getPathConfigFile($site->id);
                    if (file_exists($oldFile)) $configFile .= file_get_contents($oldFile).PHP_EOL;
                    if (file_exists($oldFileSsl)) $configFile .= file_get_contents($oldFileSsl);
                    file_put_contents($site->nginxConfigFile, $configFile);

                    d($configFile, $site);
                }*/
                break;
        }
    }
}