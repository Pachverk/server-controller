<?php


namespace Pachverk;


class Settings
{
    static $settingsFile = __DIR__ . '/../../settings.php';
    static $settings;

    static function getOption($name)
    {
        self::init();
        return self::$settings[$name];
    }

    static function setOption($code, $value)
    {
        self::init();
        self::$settings[$code] = $value;
        $data                  = '<?php return ' . PHP_EOL;
        $data                  .= var_export(self::$settings, true);
        $data                  .= ';';
        file_put_contents(self::$settingsFile, $data);
    }

    static function createSettingsFile()
    {
        if (!file_exists(self::$settingsFile)) {
            $file = '<?php return [];';
            if (!file_put_contents(self::$settingsFile, $file)) {
                Log::setError('Не удалось создать файл для конфигураций, проверьте права');
                return false;
            }
        }
        return true;
    }

    static function init()
    {
        if (!isset(self::$settings)) {
            if (self::createSettingsFile()) {
                self::$settings = require self::$settingsFile;
            }
        }
    }
}