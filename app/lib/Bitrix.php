<?php


namespace Pachverk;

class Bitrix
{
    static $siteDefaultDir = __DIR__ . '/../files/siteDir';

    /**
     * @todo Пока без проверок поскольку они реализованы шагом выше (нужно будет продублировать их)
     * @param Site $site
     * @return bool
     */
    static function createWebSpace(Site $site) {
        if (empty($site->id)) {
            Log::setError('Нужно указать ID сайта');
            return false;
        }

        if (empty($site->domains)) {
            Log::setError('Нужно указать домены сайта');
            return false;
        }

        if (empty($site->linuxUser)) {
            Log::setError('Нужно указать пользователя в системе линукс');
            return;
        }

        if (empty($site->dbUser)) {
            Log::setError('Нужно обявить пользователя базы данных');
            return;
        }

        if (empty($site->dbName)) {
            Log::setError('Нужно указать базу данных сайта');
            return;
        }

        // httpd conf
        if (1) {
            $site->httpdConfigFile = Httpd::getConfigPathDefault($site->id);
            $httpdStatus = Httpd::generateConfFile($site);

            if (!$httpdStatus) {
                Httpd::deleteHttpdConfFile($site);
                return false;
            }
        }

        // Создание окружения
        if (1 && $httpdStatus) {
            // if (empty($site->documentRoot))
            // $site->documentRoot = "/home/{$site->linuxUser}/sites/{$site->id}";
            // if (empty($site->dirSessions))
            // $site->dirSessions = "/home/{$site->linuxUser}/sessions";
            // if (empty($site->dirUploads))
            // $site->dirUploads = "/home/{$site->linuxUser}/uploads";
            $spaceStatus = $site->createSpace();
            if (!$spaceStatus) {
                $site->deleteDocumentRoot();
                Httpd::deleteHttpdConfFile($site);
            }
        }

        // Nginx Настройки
        if (1 && $spaceStatus) {
            $site->nginxConfigFile = Nginx::getPathConfigFile($site->id);
            $nginxStatus = Nginx::createNginxConfig($site);

            if (!$nginxStatus) {
                $site->deleteDocumentRoot();
                Httpd::deleteHttpdConfFile($site);
                Nginx::deleteAllConfingsBySite($site);
            }
        }

        // Создание базы данных
        if (1 && $nginxStatus) {
            $mysqlStatus = Mysql::createDatabase($site->dbName, $site->dbUser, $site->dbPassword);
        }

        // Сохранить файлы и папки по умолчанию
        if (1 && $mysqlStatus) {
            $siteDir = self::$siteDefaultDir;
            if (is_dir($siteDir)) {
                exec("cp -r {$siteDir}/* {$site->documentRoot}");
                exec("chown -R {$site->linuxUser}:{$site->linuxUser} {$site->documentRoot}");

                $file = "{$site->documentRoot}/bitrix/.settings.php";
                if (file_exists($file)) {
                    $data = file_get_contents($file);
                    $data = str_replace('#YAM_DB_NAME#', $site->dbName, $data);
                    $data = str_replace('#YAM_DB_LOGIN#', $site->dbUser, $data);
                    $data = str_replace('#YAM_DB_PASSWORD#', $site->dbPassword, $data);
                    if (file_put_contents($file, $data)) {
                        Log::setSuccess("Успешно установленны параметры для подключения к базе данных ({$file})");
                    } else {
                        Log::setSuccess("Не удалось переопределить соединение к базе данных для файла ({$file})");
                    }
                }

                $file = "{$site->documentRoot}/bitrix/php_interface/dbconn.php";
                if (file_exists($file)) {
                    $data = file_get_contents($file);
                    $data = str_replace('#YAM_DB_NAME#', $site->dbName, $data);
                    $data = str_replace('#YAM_DB_LOGIN#', $site->dbUser, $data);
                    $data = str_replace('#YAM_DB_PASSWORD#', $site->dbPassword, $data);
                    if (file_put_contents($file, $data)) {
                        Log::setSuccess("Успешно установленны параметры для подключения к базе данных ({$file})");
                    } else {
                        Log::setSuccess("Не удалось переопределить соединение к базе данных для файла ({$file})");
                    }
                }
            }
        }

        if ($mysqlStatus) {
            Mysql::addConfig($site);
        }

        return true;
    }

    static function getDataBaseInfo(Site $site) {
        // $data = file_get_contents("{$site->documentRoot}/bitrix/php_interface/dbconn.php");
        // $login = Tools::
    }

    /**
     * Пробег и оценаца состояния сайтов
     * @TODO используются методы которые нужно снести
     */
    static function monitoringSites()
    {
        /**
         * @var Site $site
         */
        $starFix = false;

        foreach (Httpd::getSiteList() as $site) {
            if (1 && !in_array($site->id, ['turkishhome.com.ua'])) continue;

            // Есть ли вообще у сайта какие-то файлы
            if (1) {
                $dir = scandir($site->documentRoot);
                if (count($dir) <= 2) {
                    $site->errors[] = 'Директория с сайтом пустая';

                    if ($starFix) {
                        $oldDir = "/home/bitrix/ext_www/".$site->id;
                        $to = dirname($site->documentRoot);
                        if (is_dir($oldDir) && is_dir($to)) {
                            exec("mv -f $oldDir $to");
                            if ($site->linuxUser) {
                                exec("chown -R {$site->linuxUser}:{$site->linuxUser} {$site->documentRoot}");
                            }
                        }
                    }
                }
            }

            // Проверка NGINX
            $site->checkNginxFiles();
            if (1 && ($ar = Nginx::getArStatus($site))) {
                if (in_array(false, $ar)) {
                    $site->errors = array_merge($site->errors, $ar);
                }

                // if ($starFix && !$site->nginxSSLConfigFile) {
                //     Nginx::createNginxSslConfigFile($site);
                // }

                // Запуск фикса
                if ($starFix) {
                    if (!$site->errors['nginxDocumentRoot'] || !$site->errors['nginxSslDocumentRoot']) {
                        $docRootConf = Nginx::getRoot($site->nginxConfigFile);
                        if ($docRootConf != $site->documentRoot) {
                            if (Nginx::saveConfig($site->nginxConfigFile)) {
                                Nginx::changeRootDir($site, $site->documentRoot, "/home/bitrix/ext_www/{$site->id}");
                                Nginx::changeRootDir($site, $site->documentRoot, $docRootConf);
                                exec('service nginx restart;');
                            }
                        }
                    }
                }
            }

            // Проверка есть ли вообще директория с сайтом
            if (1 && !is_dir($site->documentRoot)) {
                $site->errors[] = 'Нет директории с сайтом';
            }

            // Есть проблемы с файлами httpd
            if (1 && ($ar = Httpd::getArStatus($site))) {
                if (in_array(false, $ar)) {
                    $site->errors = array_merge($site->errors, $ar);
                    if (1 && !$site->errors['dirSessions']) $site->createSessionsDir();
                    if (1 && !$site->errors['dirUploads']) $site->createUploadsDir();
                }
            }

            // Фиксануть отправителя
            if (1) {
                // TODO Нужно реализовать
            }

            if (1) {
                if ($site->dirUploads != "/home/{$site->linuxUser}/uploads") {
                    $site->errors['wrongDirUploads'] = 'Диреткория для загрузок в левом месте';
                }
                if ($site->dirSessions != "/home/{$site->linuxUser}/sessions") {
                    $site->errors['wrongDirSessions'] = 'Диреткория для сессий в левом месте';
                }

                if ($starFix) {
                    if ($site->errors['wrongDirUploads']) {
                        mkdir("/home/{$site->linuxUser}/uploads", 0755, true);
                        Httpd::setUploadDir($site, "/home/{$site->linuxUser}/uploads");
                        exec("chown -R {$site->linuxUser}:{$site->linuxUser} /home/{$site->linuxUser}/uploads");
                    }

                    if ($site->errors['wrongDirSessions']) {
                        mkdir("/home/{$site->linuxUser}/sessions", 0755, true);
                        Httpd::setSessionDir($site, "/home/{$site->linuxUser}/sessions");
                        exec("chown -R {$site->linuxUser}:{$site->linuxUser} /home/{$site->linuxUser}/sessions");
                    }

                    if ($site->errors['wrongDirUploads'] || $site->errors['wrongDirSessions']) {
                        Httpd::restart();
                    }
                }
            }

            // Фикс ServerAdmin
            if (1 && strpos($site->domains, 'ServerAdmin') !== false) {
                $site->errors[] = 'У сайта в название доменов влетело лишнее, нужно проверить целостность сайта';
                if ($starFix) {
                    Httpd::saveConfig($site->httpdConfigFile);
                    $data = file_get_contents($site->httpdConfigFile);
                    $data = str_replace('ServerAdmin', "\n\tServerAdmin", $data);
                    file_put_contents($site->httpdConfigFile, $data);
                }
            }

            // Сайт не в родной директории
            if (1 && strpos($site->documentRoot, 'ext_www') !== false) {
                $site->errors[] = 'Сайт находится в потенциально опасно директории';

                if ($starFix) {
                    $site->linuxUser = Site::getDefaultUsetNameByID($site->id);
                    $newDocumentRoot = "/home/{$site->linuxUser}/sites/{$site->id}";

                    Linux::createUser($site->linuxUser);

                    mkdir($newDocumentRoot, 755, true);
                    Httpd::changeRootDir($site->httpdConfigFile, $site->documentRoot, $newDocumentRoot);
                    Nginx::changeRootDir($site, $newDocumentRoot);

                    exec("mv -f {$site->documentRoot} /home/{$site->linuxUser}/sites");
                    exec("chown -R {$site->linuxUser}:{$site->linuxUser} /home/{$site->linuxUser}");

                    Httpd::setITKConf($site, $site->linuxUser);


                    Httpd::restart();
                    Nginx::restart();
                }
            }

            // Сайт имеет связки
            if (1 && is_link($site->documentRoot.'/bitrix')) {
                $site->warnings['siteHasLink'] = 'Сайт имеет связку с другим сайтом на сервере';
            }

            if (1 && !empty($site->errors)) {
                d($site);
                break;
            }

            d($site);
        }
    }

    static function changeDocuentRootSites()
    {
        /**
         * @var string $linuxUserName     Имя пользователя в системе
         * @var Site   $site
         * @var bool   $globalStausAction Определение общего статуса можно ли продолжать цепочку событий
         */

        $sites = Httpd::getSiteList();
        $i     = 0;

        foreach ($sites as $site) {
            $globalStausAction = true;

            if (empty($site->documentRoot)
                || strpos($site->documentRoot, '/home/bitrix/ext_www/') === false) continue;

            // Опредилить слинкованые сайты
            if (1) {
                if (is_link($site->documentRoot.'/bitrix')) {
                    // echo $site->documentRoot . PHP_EOL;
                    continue;
                }
            }

            // Создание пользователей для сервера
            if (1 && $globalStausAction) {
                $linuxUserName     = Users::getDefaultUserName($site);
                $globalStausAction = Users::createUser($linuxUserName);
            }

            // Сохранение и изменение параметров httpd
            if (1 && $globalStausAction) {
                $globalStausAction = Httpd::changeRootDir(
                    $site->httpdConfigFile,
                    $site->documentRoot,
                    "/home/{$linuxUserName}/sites/{$site->id}"
                );

                if ($globalStausAction) {
                    $globalStausAction = Httpd::setITKConf($site, $linuxUserName);
                }

                if ($globalStausAction) {
                    $globalStausAction = Httpd::setSessionDir($site, "/home/{$linuxUserName}/sessions");
                }

                if ($globalStausAction) {
                    $globalStausAction = Httpd::setUploadDir($site, "/home/{$linuxUserName}/uploads");
                }
            }

            // Сохранение и изменение параметров nginx
            if (1 && $globalStausAction) {
                $site->checkNginxFiles();
                $globalStausAction = Nginx::changeRootDir($site, "/home/{$linuxUserName}/sites/{$site->id}");
            }

            // Копирование сайта с одной директории в другую
            if (1 && $globalStausAction) {
                mkdir("/tmp/old_sites", 755, true);
                mkdir("/home/{$linuxUserName}/sites/{$site->id}", 755, true);
                mkdir("/home/{$linuxUserName}/sites/uploads", 755, true);
                mkdir("/home/{$linuxUserName}/sites/sessions", 755, true);
                // exec("cp -r {$site->documentRoot} /tmp/old_sites");
                exec("mv {$site->documentRoot} /home/{$linuxUserName}/sites");
                exec("chown -R {$linuxUserName}:{$linuxUserName} /home/{$linuxUserName}");
                exec("chmod 0 /home/{$linuxUserName}");
                exec("chmod 0700 /home/{$linuxUserName}");

                if (!is_dir("/home/{$linuxUserName}/sites/{$site->id}")
                    || !is_dir("/home/{$linuxUserName}/sites/uploads")
                    || !is_dir("/home/{$linuxUserName}/sites/sessions")
                ) {
                    $globalStausAction = false;
                }
            }

            if ($globalStausAction === false) {
                Httpd::rollBack($site);
                Nginx::rollBack($site);
            }

            echo "{$site->id}: {$site->domains}".PHP_EOL;
        }

        Log::printAll();

    }

    static function findVirusInSites()
    {
        $sites = Httpd::getSiteList();

        $virusFiles = [
            // '/bitrix/modules/main/include.php',
            '/bitrix/js/main/core/core_loader.js',
            '/bitrix/js/main/core/core_tasker.js',
            '/bitrix/gadgets/bitrix/weather/lang/ru/exec/include.php',
            '/bitrix/tools/check_files.php',
            '/restore.php',
        ];

        /** @var Site $site */
        foreach ($sites as $site) {
            echo 'Проверка сайта: '.$site->id.PHP_EOL;

            $virusExist = false;
            foreach ($virusFiles as $file) {
                if (file_exists($site->documentRoot.$file)) {
                    $virusExist = true;
                    echo $site->documentRoot.$file.PHP_EOL;
                }
            }
        }
    }

    static function deleteCaches()
    {
        /**
         * @var Site $site
         */
        $allSites = Httpd::getSiteList();
        foreach ($allSites as $site) {
            if (empty($site->documentRoot)) continue;
            exec("rm -rf {$site->documentRoot}/bitrix/cache");
            exec("rm -rf {$site->documentRoot}/bitrix/managed_cache");
            exec("rm -rf {$site->documentRoot}/bitrix/stack_cache");
        }
    }

    static function deleteBackups()
    {
        /**
         * @var Site $site
         */
        $allSites = Httpd::getSiteList();
        foreach ($allSites as $site) {
            if (empty($site->documentRoot)
                || !is_dir($site->documentRoot.'/bitrix/backup')
            ) continue;
            exec("rm -rf {$site->documentRoot}/bitrix/backup/*");
        }
    }
}