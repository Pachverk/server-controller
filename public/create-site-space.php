<?php
/**
 * @var \Pachverk\Server $server
 */
include 'bootstrap.php';
$bitrix  = new \Pachverk\Bitrix();
$servers = $bitrix->getServers();
d($_REQUEST);
?>
<div class="row">
    <div class="container-fluid">
        <div class="col-12">
            <form method="post">

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Сервер куда будет происходить установка:</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="server">
                            <option>Выберите сервер для создания окружения</option>
                            <? foreach ($servers as $server): ?>
                                <option <?=($_REQUEST['server'] === $server->ip)?'selected':''?> value="<?=$server->ip?>"><?=$server->name?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">ID Сайта:</label>
                    <div class="col-sm-10">
                        <input name="id" type="text" class="form-control"
                               value="<?=$_REQUEST['id']?>"
                               placeholder="Уникальное название сайта (Папка сайта)">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Укажите имя пользователя линукс:</label>
                    <div class="col-sm-10">
                        <input name="linuxUser"
                               type="text" class="form-control"
                               value="<?=$_REQUEST['linuxUser']?>"
                               placeholder="Название пользователя linux системе">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Домены сайта:</label>
                    <div class="col-sm-10">
                        <? if (!empty($_REQUEST['domains'])): ?>
                            <? foreach ($_REQUEST['domains'] as $domain): ?>
                                <div class="form-group multiply">
                                    <input name="domains[]"
                                           type="text"
                                           value="<?=$domain?>"
                                           class="form-control"
                                           placeholder="Домен сайта">
                                </div>
                            <? endforeach; ?>
                        <? endif; ?>
                        <div class="form-group multiply">
                            <input name="domains[]"
                                   type="text"
                                   class="form-control"
                                   placeholder="Домен сайта">
                        </div>
                    </div>
                </div>

                <button class="btn btn-success">Success</button>

            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.multiply').each(function () {
            var parent = $(this).parent();
            if (!parent.data('multiply')) {
                parent.append($('<a>', {
                    text: 'Add',
                    class: 'addField',
                    style: 'margin-right: 10px;',
                    href: 'javascript: void(0)'
                }));
                parent.append($('<a>', {
                    text: 'Remove',
                    class: 'removeField',
                    style: 'margin-right: 10px;',
                    href: 'javascript: void(0)'
                }));
                parent.data('multiply', true);
            }
        });
        $('.addField').click(function (e) {
            e.preventDefault();
            $(this).prev().clone().insertBefore(this);
        });
        $('.removeField').click(function (e) {
            e.preventDefault();
            var elem = $(this).prev().prev();
            elem.remove();
        });
    });
</script>
